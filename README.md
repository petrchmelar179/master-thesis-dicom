# Master thesis DICOM
This repository contains part of my master thesis which deals with:

*  Implementation of abstraction over [fo-dicom](https://github.com/fo-dicom/fo-dicom) library
*  Implementation of averaging after registration and reconstruction of High Dynamic Range images from Low Dynamic Range Images
*  Implementation of a service which is providing image processing methods using gRPC by google

The repository is separated into the two parts:

*  [Improc part](https://gitlab.com/petrchmelar179/master-thesis-dicom/tree/master/dicom) with implementations of image processing methods and a improc service server
*  [Dicom part](https://gitlab.com/petrchmelar179/master-thesis-dicom/tree/master/improc) with implementations of Fo-Dicom abstraction and Sample Application with built-in an improc service client

Dicom sample app loads series of dicom files using implemented abstraction.
Then loaded files are processed by image processing service providing AAR, HDR calibration, and HDR services.

## Releases

The sample app is released as a executable for windows, Linux, and osx on my Dropbox [releases](https://www.dropbox.com/sh/tam3xayyr2lf1m8/AABlPLK2MFpvY_Iy7A4qcPIqa?dl=0)


The image processing service is released as docker image which is available on the [docker hub](https://hub.docker.com/r/pchmelar/improc-service/)

## Running Sample app along with improc service

To be able to run the sample application with the image processing service you need to run the image processing service at first.
The easiest way to run the image processing service is running it as pre-built Docker image in Docker container.

### Running improc service in the container
#### 1. Installation of Docker
At first, the docker has to be downloaded and installed on the host system from [Docker download page](https://www.docker.com/get-docker). 
For more information about docker installation visit [Docker documentation](https://docs.docker.com/install/#docker-certified-infrastructure)

#### 2. Running Improc docker image in Docker container
After docker installation, there should be available docker command from command line. 
The Image processing service should be launched by executing `docker run -it -p 50052:50052 pchmelar/improc-service`.
The docker should automatically pull the newest version of a docker image from [Docker hub](https://hub.docker.com/r/pchmelar/improc-service/) and execute it.
Execution output should look as follows (without pulling messages):

```console
docker run -it -p 50052:50052 pchmelar/improc-service
Starting improc service server...
E0427 12:29:26.341699000       5 tcp_server_posix.cc:71]     check for SO_REUSEPORT: {"created":"@1524832166.341686100","description":"SO_REUSEPORT unavailable on compiling system","file":"src/core/lib/iomgr/socket_utils_common_posix.cc","file_line":165}
Improc service server started.
2018-04-27 12:29:26.346808 Periodical beat
2018-04-27 12:29:31.352018 Periodical beat
2018-04-27 12:29:36.357201 Periodical beat
```

### Running sample app
With improc service, the abilities of the image processing service and the Dicom abstraction can be tested using the Sample app.
The sample app executables can be downloaded using links below.
In zip archives with executables, there are also zipped sample DICOM data, which can be used as input for the Sample application.
All sample data and result can be viewed by any dicom images viewer.
For example, there is available free [Micro Dicom viewer](http://www.microdicom.com)

#### Showing help screen
To show help screen just execute executable with `--help` argument

```
λ Eledus.Sciox.Dicom.SampleApp.exe --help
Eledus.Sciox.Dicom 1.0.0
Copyright (C) 2018 Eledus.Sciox.Dicom.SampleApp

  aar         Apply averaging after registration on dicom files series.

  hdrcalib    Calibrate HDR service with data.

  hdr         Apply HDR service on dicom files series.

  help        Display more information on a specific command.

  version     Display version information.
```

#### Running averaging after registration
To run AAR service with noised_data samples and store data to result.dcm just execute:

```
λ Eledus.Sciox.Dicom.SampleApp.exe aar --input noised_images\ --output result.dcm
[14:38:39 INF] GRPC client initalized.
[14:38:39 INF] aar parameters succesfully parsed.
[14:38:39 INF] Loading noised_images\[179]60kV-250uA-80ms.dcm
[14:38:39 INF] File noised_images\[179]60kV-250uA-80ms.dcm was succesfully loaded
...
...
[14:38:39 INF] Loading noised_images\[180]60kV-250uA-80ms.dcm
[14:38:39 INF] File noised_images\[179]60kV-250uA-80ms.dcm was succesfully loaded
[14:38:40 INF] Calling AAR request on improc service.
[14:38:46 INF] AAR request completed with succesfull response.
[14:38:46 INF] Creating new DICOM data from AAR response.
[14:38:46 INF] New DICOM data was succesfully created from AAR response.
[14:38:46 INF] Creating new file: result.dcm.
[14:38:46 INF] New file result.dcm was succesfully created.
[14:38:46 INF] Writing new DICOM data to result.dcm.
[14:38:46 INF] New DICOM data was succesfully written to result.dcm.
```

#### Running HDR reconstruction
For HDR reconstruction there has to be executed two commands.
At first the calibration has to be proccesed:

```
λ Eledus.Sciox.Dicom.SampleApp.exe hdrcalib --input hdr_calibration_images\
[14:42:03 INF] GRPC client initalized.
[14:42:03 INF] hdrcalib parameters succesfully parsed.
[14:42:03 INF] Loading hdr_calibration_images\1000ms.dcm
[14:42:04 INF] File hdr_calibration_images\1000ms.dcm was succesfully loaded
...
...
[14:42:04 INF] Loading hdr_calibration_images\60ms.dcm
[14:42:04 INF] File hdr_calibration_images\60ms.dcm was succesfully loaded
[14:42:04 INF] Calling HDR calibration request on improc service.
[14:42:09 INF] HDR calibration request completed with succesfull response.
[14:42:09 INF] HDR calibration succesfully done.
```

After calibration LDR images can be marged to HDR image by calling:


```
λ Eledus.Sciox.Dicom.SampleApp.exe hdr --input ldr_images\ --output hdr_result.dcm
[14:43:42 INF] GRPC client initalized.
[14:43:42 INF] hdr parameters succesfully parsed.
[14:43:42 INF] Loading ldr_images\1000ms.dcm
[14:43:43 INF] File ldr_images\1000ms.dcm was succesfully loaded
...
...
[14:43:43 INF] Loading ldr_images\60ms.dcm
[14:43:43 INF] File ldr_images\60ms.dcm was succesfully loaded
[14:43:43 INF] Calling HDR request on improc service.
[14:43:45 INF] HDR request completed with succesfull response.
[14:43:45 INF] Creating new DICOM data from HDR response.
[14:43:46 INF] New DICOM data was succesfully created from HDR response.
[14:43:46 INF] Creating new file: hdr_result.dcm.
[14:43:46 INF] New file hdr_result.dcm was succesfully created.
[14:43:46 INF] Writing new DICOM data to hdr_result.dcm.
[14:43:46 INF] New DICOM data was succesfully written to hdr_result.dcm.
```

## Improc part
Whole improc part is developed using python.
In this part, there are developed image processing methods which can be executed by calling gRPC service.
The whole service can be executed either using [Docker image]() or [anaconda](https://anaconda.org).

### Running using docker image
After installation of docker which can be downloaded from [download page](https://www.docker.com/get-docker) the docker image can be started in the container using:

> docker run -it -p 50052:50052 pchmelar/improc-service


### Running using conda
After installation of conda python interpreter which can be downloaded from [download page](https://www.anaconda.com/downloads) a conda virtual environment can be created using:

> λ conda env create -f environment.yaml

The command has to be executed from enviroment.yaml file location. (\master-thesis-dicom\improc)
After environment installation it can be activated using:

> λ activate improc-env

With envirnment active the image processing service server can be started as follows:

> (improc-env) λ python start_improc_service.py

## Dicom part
The following section describes Dicom part of the repository.

### Eledus.Sciox.Dicom
The project containing all of abstraction files for Fo-Dicom.
The library is targetting .NET Standard 2.0 so it can be used in the most project based on .NET implementation.
See [.NET standard compatibility table](https://github.com/dotnet/standard/blob/master/docs/versions.md) for more information about compatibility.
This project should be deployed as NuGet package to the official NuGet server in the future.

### Eledus.Sciox.Dicom.SampleApp
The project which demonstrates Eledus.Sciox.Dicom library and improc service features.
The project is targetting .NET Core 2.0, so it can be build using .NET Core 2.0 SDK.
Built executable can be used as a client for image processing service server.

### Eledus.Sciox.Dicom.Test
File containing unit and integration test for Eledus.Sciox.Dicom library.

### Building Eledus.Sciox.Dicom.SampleApp from source
#### 1. .NET Core SDK installation
The project is targetting .NET Core 2.0 so there has to be installed .NET Core 2.0 SDK to be able to run and build the sample app project.
There are some possible ways to install the SDK:

1.  Chocolatey
   - Run following command with [chocolatey](https://chocolatey.org/) installed to install [.NET Core SDK package](https://chocolatey.org/packages/dotnetcore-sdk/2.1.104):



```console
λ choco install dotnetcore-sdk
```

2.  Visual studio installer
   - Install .NET cross-platform development toolset in modifying window
3.  Official web site
   - Go to [SDK downloading page](https://www.microsoft.com/net/download/windows) and download and install SDK

Successful installation can be verified by running:

```console
λ dotnet --version
2.1.101
```

#### 2. Executing sample application
The application can be built and executed from the command line by .NET Core SDK or from Visual Studio.
The application communicates with improc service server so the server should be running for proper functionality of the application.

##### Executing directly by .NET Core SDK
First, thing one need to do is navigate to Eledus.Sciox.Dicom.SampleApp directory.
After that, the only thing to do for a build is running `dotnet build` command.
It may take some time because the build process will be downloading all of dependencies using NuGet package manager.
The build command should automatically build all dependencies from solution (with abstraction library of course).

A successful build can be verified by executing app without parameters.
Sample app should print out basic help screen because no file arguments were delivered.

```console
λ dotnet run
Using launch settings from C:\Users\petrc\Repositories\master-thesis-dicom\dicom\Eledus.Sciox.Dicom.SampleApp\Properties\launchSettings.json...
Eledus.Sciox.Dicom 1.0.0
Copyright (C) 2018 Eledus.Sciox.Dicom.SampleApp
ERROR(S):
No verb selected.

  aar         Apply averaging after registration on dicom files series.

  hdrcalib    Calibrate HDR service with data.

  hdr         Apply HDR service on dicom files series.

  help        Display more information on a specific command.

  version     Display version information.
```

Now one can execute sample app by running it with a verb and its arguments.

```
λ dotnet run hdrcalib --input hdr_calibration_images\
Using launch settings from C:\Users\petrc\Repositories\master-thesis-dicom\dicom\Eledus.Sciox.Dicom.SampleApp\Properties\launchSettings.json...
[14:56:28 INF] GRPC client initalized.
[14:56:28 INF] hdrcalib parameters succesfully parsed.
[14:56:28 INF] Loading hdr_calibration_images\1000ms.dcm
[14:56:29 INF] File hdr_calibration_images\1000ms.dcm was succesfully loaded
...
...
[14:56:29 INF] Loading hdr_calibration_images\60ms.dcm
[14:56:29 INF] File hdr_calibration_images\60ms.dcm was succesfully loaded
[14:56:29 INF] Calling HDR calibration request on improc service.
[14:56:34 INF] HDR calibration request completed with succesfull response.
[14:56:34 INF] HDR calibration succesfully done.

```

##### Executing from Visual studio
The app can be executed from visual studio using standard steps.
Just click on start without debugging in debug menu or press ctrl + F5.
The project should build and run automatically with predefined samples as arguments.
Project arguments can be changed in the launchSettings.json file.