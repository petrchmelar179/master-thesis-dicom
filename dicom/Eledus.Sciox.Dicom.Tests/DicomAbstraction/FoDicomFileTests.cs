﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Eledus.Sciox.Dicom.DicomAbstraction;
using Moq;
using Microsoft.Extensions.FileProviders;
using System.IO;
using System.Linq;
using Dicom;
using Dicom.Imaging;
using Dicom.IO.Buffer;
using System.Threading.Tasks;

namespace Eledus.Sciox.Dicom.Tests
{
    public class FoDicomFileTests
    {
        [Fact]
        public async Task OpenLoadsDataToModel()
        {
            var modelMock = new Mock<IDicomModel<IDicomDatasetWrapper>>();
            var fileMock = new Mock<IDicomFileWrapper>();
            var streamProviderMock = new Mock<IFileStreamProvider>();
            var datasetMock = new Mock<IDicomDatasetWrapper>();

            fileMock.Setup(m => m.OpenAsync(It.IsAny<IFileStreamProvider>()))
                .Returns(Task.FromResult(datasetMock.Object));

            var dicomFile = new FoDicomDicomFile(modelMock.Object, fileMock.Object);
            await dicomFile.Open(streamProviderMock.Object);

            modelMock.Verify(m => m.Update(
                It.Is<IDicomDatasetWrapper>(i => i == datasetMock.Object)),
                Times.Once);
        }

        [Fact]
        public async Task OpenOpensFileWithStreamProvider()
        {
            var modelMock = new Mock<IDicomModel<IDicomDatasetWrapper>>();
            var fileMock = new Mock<IDicomFileWrapper>();
            var streamProviderMock = new Mock<IFileStreamProvider>();
            var datasetMock = new Mock<IDicomDatasetWrapper>();
            var dicomFile = new FoDicomDicomFile(modelMock.Object, fileMock.Object);

            await dicomFile.Open(streamProviderMock.Object);
            fileMock.Verify(m => m.OpenAsync(It.Is<IFileStreamProvider>(p => p == streamProviderMock.Object)),
                Times.Once());
        }

        [Fact]
        public async Task SaveFileSaveUseCorrectDatasetForSave()
        {
            var modelMock = new Mock<IDicomModel<IDicomDatasetWrapper>>();
            var fileMock = new Mock<IDicomFileWrapper>();
            var streamProviderMock = new Mock<IFileStreamProvider>();
            var datasetMock = new Mock<IDicomDatasetWrapper>();

            modelMock.Setup(m => m.GetDataset())
                .Returns(datasetMock.Object);

            var dicomFile = new FoDicomDicomFile(modelMock.Object, fileMock.Object);
            await dicomFile.Save(streamProviderMock.Object);

            fileMock.Verify(m => m.SaveAsync(It.IsAny<IFileStreamProvider>(),
                It.Is<IDicomDatasetWrapper>(w => w == datasetMock.Object)),
                Times.Once);
        }

        [Fact]
        public async Task SaveFileSaveDatasetToPassedFiletreamProvider()
        {
            var modelMock = new Mock<IDicomModel<IDicomDatasetWrapper>>();
            var fileMock = new Mock<IDicomFileWrapper>();
            var streamProviderMock = new Mock<IFileStreamProvider>();

            var dicomFile = new FoDicomDicomFile(modelMock.Object, fileMock.Object);
            await dicomFile.Save(streamProviderMock.Object);

            fileMock.Verify(m => m.SaveAsync(
                It.Is<IFileStreamProvider>(fs => fs == streamProviderMock.Object),
                It.IsAny<IDicomDatasetWrapper>()), Times.Once);
        }
    }
}
