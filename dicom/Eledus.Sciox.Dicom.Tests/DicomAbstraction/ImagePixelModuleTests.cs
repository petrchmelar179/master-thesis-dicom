﻿using Dicom;
using Dicom.Imaging;
using Dicom.IO.Buffer;
using Eledus.Sciox.Dicom.DicomAbstraction;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace Eledus.Sciox.Dicom.Tests
{
    public class ImagePixelModuleTests
    {
        [Fact]
        public void AllPropertiesCanBeSet()
        {
            var pixelDataMock = new Mock<IDicomPixelDataWrapper>();
            var module = new FoDicomImagePixelModule(pixelDataMock.Object)
            {
                BitsAllocated = 5,
                BitsStored = 5,
                Columns = 5,
                HighBit = 5,
                PhotometricType = PhotometricInterpretationType.YbrFull,
                PixelRepresentation = 5,
                Rows = 5,
                SamplesPerPixel = 5
            };
            module.PixelData.Add(new byte[] { 0, 1, 0 });

            Assert.Equal(5, module.BitsAllocated);
            Assert.Equal(5, module.BitsStored);
            Assert.Equal(5, module.Columns);
            Assert.Equal(5, module.HighBit);
            Assert.Equal(PhotometricInterpretationType.YbrFull, module.PhotometricType);
            Assert.Equal(new List<byte[]>() { new byte[] { 0, 1, 0 } }, module.PixelData);
            Assert.Equal(5, module.PixelRepresentation);
            Assert.Equal(5, module.Rows);
            Assert.Equal(5, module.SamplesPerPixel);
        }

        [Fact]
        public void GetDatasetUpdatesNewDatasetWithCorrectData()
        {
            var pixelDataMock = new Mock<IDicomPixelDataWrapper>();
            var module = new FoDicomImagePixelModule(pixelDataMock.Object)
            {
                BitsAllocated = 5,
                BitsStored = 5,
                Columns = 5,
                HighBit = 5,
                PhotometricType = PhotometricInterpretationType.YbrFull,
                PixelRepresentation = 5,
                Rows = 5,
                SamplesPerPixel = 5
            };
            module.PixelData.Add(new byte[] { 1, 0, 1});

            IDicomDatasetWrapper updatedDataset = null;
            pixelDataMock.Setup(m => m.Create(It.IsAny<IDicomDatasetWrapper>(), It.IsAny<bool>()))
                .Callback<IDicomDatasetWrapper, bool>((dataset, clearData) => updatedDataset = dataset);

            var moduleDataset = module.GetDataset();
            Assert.Equal(5, updatedDataset.Get(DicomTag.BitsAllocated, (ushort)0));
            Assert.Equal(5, updatedDataset.Get(DicomTag.BitsStored, (ushort)0));
            Assert.Equal(5, updatedDataset.Get(DicomTag.Columns, (ushort)0));
            Assert.Equal(5, updatedDataset.Get(DicomTag.HighBit, (ushort)0));
            Assert.Equal(5, updatedDataset.Get(DicomTag.PixelRepresentation, (ushort)0));
            Assert.Equal(5, updatedDataset.Get(DicomTag.Rows, (ushort)0));
            Assert.Equal(5, updatedDataset.Get(DicomTag.SamplesPerPixel, (ushort)0));
            Assert.Equal(PhotometricInterpretation.YbrFull.Value, updatedDataset.Get(DicomTag.PhotometricInterpretation, ""));
            pixelDataMock.Verify(m => m.AddFrame(It.Is<byte[]> (f => f == module.PixelData[0])), Times.Once);
        }
        
        [Fact]
        public void MissingRequiredTagThrowsInvalidDataExceptionOnUpdate()
        {
            var pixelDataMock = new Mock<IDicomPixelDataWrapper>();
            var module = new FoDicomImagePixelModule(pixelDataMock.Object);
            var datasetMock = new Mock<IDicomDatasetWrapper>();
            datasetMock
                .Setup(m => m.Contains(It.Is<DicomTag>(t => module.RequiredTags.Contains(t))))
                .Returns(false);
            Assert.Throws<InvalidDataException>(() => module.Update(datasetMock.Object));
        }

        [Fact]
        public void UpdateModelUpdatesModuleWithDatasetData()
        {
            var pixelDataMock = new Mock<IDicomPixelDataWrapper>();
            var datasetMock = new Mock<IDicomDatasetWrapper>();

            datasetMock.Setup(m => m.Contains(It.IsAny<DicomTag>())).Returns(true);
            datasetMock.Setup(m => m.Get(It.IsAny<DicomTag>(),
                It.IsAny<PhotometricInterpretation>()))
                .Returns(PhotometricInterpretation.Monochrome2);
            datasetMock.Setup(m => m.Get(It.IsAny<DicomTag>(), It.IsAny<ushort>()))
                .Returns(10);
            datasetMock.Setup(m => m.Get(It.IsAny<DicomTag>(), It.IsAny<int>()))
                .Returns(2);

            pixelDataMock.Setup(m => m.GetFrame(It.IsIn(new int[] { 0, 1 })))
                .Returns(new byte[] { 0, 1, 0 });
            pixelDataMock.SetupGet(m => m.NumberOfFrames).Returns(2);

            var module = new FoDicomImagePixelModule(pixelDataMock.Object);
            module.Update(datasetMock.Object);

            Assert.Equal(10, module.BitsAllocated);
            Assert.Equal(10, module.BitsStored);
            Assert.Equal(10, module.Columns);
            Assert.Equal(10, module.HighBit);
            Assert.Equal(PhotometricInterpretationType.Monochrome2, module.PhotometricType);
            Assert.Equal(new List<byte[]> { new byte[] { 0, 1, 0 }, new byte[] { 0, 1, 0 } },
                module.PixelData);
            Assert.Equal(10, module.PixelRepresentation);
            Assert.Equal(10, module.Rows);
            Assert.Equal(10, module.SamplesPerPixel);
        }

        [Fact]
        public void UpdateModuleThrowsInvaliDdataExceptionOnUnsuportedData()
        {
            
            var datasetMock = new Mock<IDicomDatasetWrapper>();
            datasetMock.Setup(m => m.Contains(It.IsAny<DicomTag>())).Returns(true);
            datasetMock.Setup(m => m.Get(It.IsAny<DicomTag>(), It.IsAny<PhotometricInterpretation>()))
                .Returns(PhotometricInterpretation.Monochrome2);

            var pixelDataMock = new Mock<IDicomPixelDataWrapper>();
            pixelDataMock.Setup(m => m.Create(
                It.Is<IDicomDatasetWrapper>(d => d == datasetMock.Object),
                It.IsAny<bool>()))
                .Throws(new DicomImagingException(""));

            var module = new FoDicomImagePixelModule(pixelDataMock.Object);

            Assert.Throws<InvalidDataException>(() => module.Update(datasetMock.Object));
        }

        [Fact]
        public void UpdateModuleThrowsInvaliddataExceptionOnBitsAllocatedGreaterThan16()
        {
            var datasetMock = new Mock<IDicomDatasetWrapper>();
            datasetMock.Setup(m => m.Contains(It.IsAny<DicomTag>())).Returns(true);
            datasetMock.Setup(m => m.Get(It.IsAny<DicomTag>(), It.IsAny<PhotometricInterpretation>()))
                .Returns(PhotometricInterpretation.Monochrome2);
            datasetMock.Setup(m => m.Get(It.Is<DicomTag>(tag => tag == DicomTag.BitsAllocated),
                It.IsAny<ushort>()))
                .Returns(17);

            var pixelDataMock = new Mock<IDicomPixelDataWrapper>();

            var module = new FoDicomImagePixelModule(pixelDataMock.Object);

            Assert.Throws<InvalidDataException>(() => module.Update(datasetMock.Object));
        }

        [Fact]
        public void UpdateModuleCreatesNewFramesInPixelData()
        {
            var datasetMock = new Mock<IDicomDatasetWrapper>();
            datasetMock.Setup(m => m.Contains(It.IsAny<DicomTag>())).Returns(true);
            datasetMock.Setup(m => m.Get(It.IsAny<DicomTag>(), It.IsAny<PhotometricInterpretation>()))
                .Returns(PhotometricInterpretation.Monochrome2);

            var pixelDataMock = new Mock<IDicomPixelDataWrapper>();

            var module = new FoDicomImagePixelModule(pixelDataMock.Object);
            module.Update(datasetMock.Object);

            pixelDataMock.Verify(
                m => m.Create(It.Is<IDicomDatasetWrapper>(d => d == datasetMock.Object),
                It.Is<bool>(b => !b)), Times.Once);
        }

        [Fact]
        public void UnsuportedPhotometricRepresentationThrowsInvalidDataExceptionOnUpdate()
        {
            var pixelDataMock = new Mock<IDicomPixelDataWrapper>();
            var module = new FoDicomImagePixelModule(pixelDataMock.Object);
            var datasetMock = new Mock<IDicomDatasetWrapper>();
            datasetMock.Setup(m => m.Contains(It.IsAny<DicomTag>())).Returns(true);
            datasetMock.Setup(m => m.Get(It.IsAny<DicomTag>(), It.IsAny<PhotometricInterpretation>()))
                .Returns(PhotometricInterpretation.YbrRct);

            Assert.Throws<InvalidDataException>(() => module.Update(datasetMock.Object));
        }

        [Fact]
        public void ImagePixelModulCanBeInitializedWithoutPixelData()
        {
            var module = new FoDicomImagePixelModule();
        }
    }
}
