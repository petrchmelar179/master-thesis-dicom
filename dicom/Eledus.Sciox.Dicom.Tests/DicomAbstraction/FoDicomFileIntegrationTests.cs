﻿using Dicom;
using Dicom.Imaging;
using Dicom.IO.Buffer;
using Eledus.Sciox.Dicom.DicomAbstraction;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Eledus.Sciox.Dicom.Tests.DicomAbstraction
{
    public class FoDicomFileIntegrationTests
    {
        [Fact]
        public async Task CanCreateNewDicomFileWithLengthGreaterThanPixelData()
        {                
            var fileInfo = new FileInfo("test.dcm");
            new FileStream(fileInfo.FullName, FileMode.Create).Close();

            var physicFileProvider = new PhysicalFileProvider(fileInfo.DirectoryName);
            var fileProvider = new PhysicalFileStreamProvider(physicFileProvider.GetFileInfo(fileInfo.Name));
            var dicomFile = new FoDicomDicomFile(new FoDicomModel(), new DicomFileProxy());

            dicomFile.Model.ImagePixelModule.BitsAllocated = 8;
            dicomFile.Model.ImagePixelModule.BitsStored = 8;
            dicomFile.Model.ImagePixelModule.HighBit = 7;
            dicomFile.Model.ImagePixelModule.PhotometricType = PhotometricInterpretationType.Monochrome2;
            dicomFile.Model.ImagePixelModule.PixelRepresentation = 0;
            dicomFile.Model.ImagePixelModule.SamplesPerPixel = 1;
            dicomFile.Model.ImagePixelModule.Rows = 512;
            dicomFile.Model.ImagePixelModule.Columns = 512;

            var random = new Random();
            var frame = new byte[512 * 512];

            foreach (var randomFrame in Enumerable.Range(0, 4))
            {
                random.NextBytes(frame);
                dicomFile.Model.ImagePixelModule.PixelData.Add(frame.ToArray());
            }

            await dicomFile.Save(fileProvider);
            await fileProvider.SaveStreamAsync();
            fileProvider.Dispose();

            using (var fs = new FileStream(fileInfo.FullName, FileMode.Open))
            {
                Assert.True(fs.Length > 512 * 512 * 4);
            }

            File.Delete(fileInfo.FullName);
        }

        [Fact]
        async Task CanLoadMultiFrameDicomFile()
        {
            var fileInfo = new FileInfo("sample_multiframe.dcm");
            var physicFileProvider = new PhysicalFileProvider(fileInfo.DirectoryName);
            var fileProvider = new PhysicalFileStreamProvider(physicFileProvider.GetFileInfo(fileInfo.Name));
            var dicomFile = new FoDicomDicomFile(new FoDicomModel(), new DicomFileProxy());
            await dicomFile.Open(fileProvider);

            Assert.Equal(17, dicomFile.Model.ImagePixelModule.PixelData.Count);
        }

        [Fact]
        async Task CanLoadOneFrameDicomFile()
        {
            var fileInfo = new FileInfo("sample_one_frame.dcm");
            var physicFileProvider = new PhysicalFileProvider(fileInfo.DirectoryName);
            var fileProvider = new PhysicalFileStreamProvider(physicFileProvider.GetFileInfo(fileInfo.Name));
            var dicomFile = new FoDicomDicomFile(new FoDicomModel(), new DicomFileProxy());
            await dicomFile.Open(fileProvider);

            Assert.Equal(1, dicomFile.Model.ImagePixelModule.PixelData.Count);
        }
    }
}
