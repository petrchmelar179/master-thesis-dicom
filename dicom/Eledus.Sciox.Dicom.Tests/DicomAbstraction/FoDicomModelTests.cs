﻿using Eledus.Sciox.Dicom.DicomAbstraction;
using Moq;
using Xunit;

namespace Eledus.Sciox.Dicom.Tests.DicomAbstraction
{
    public class FoDicomModelTests
    {
        [Fact]
        void InitializeImagePixelModulePropertyWithCorrectData()
        {
            var imagePixelModule = new Mock<IImagePixelModule<IDicomDatasetWrapper>>();
            var model = new FoDicomModel(imagePixelModule.Object);
            Assert.Equal(imagePixelModule.Object, model.ImagePixelModule);
        }

        [Fact]
        void UpdateUpdatesImagePixelModuleWithDataset()
        {
            var datasetMock = new Mock<IDicomDatasetWrapper>();
            var imagePixelModuleMock = new Mock<IImagePixelModule<IDicomDatasetWrapper>>();
            var model = new FoDicomModel(imagePixelModuleMock.Object);

            model.Update(datasetMock.Object);
            imagePixelModuleMock.Verify(m => m.Update(
                It.Is<IDicomDatasetWrapper>(d => d == datasetMock.Object)), Times.Once);
        }
    }
}
