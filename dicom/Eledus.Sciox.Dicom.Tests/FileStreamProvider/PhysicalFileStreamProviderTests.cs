﻿using Eledus.Sciox.Dicom.DicomAbstraction;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.FileProviders.Physical;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace Eledus.Sciox.Dicom.Tests
{
    public class PhysicalFileStreamProviderTests : IDisposable
    {
        IFileInfo _fileInfo;
        Stream _stream;
        public PhysicalFileStreamProviderTests()
        {
            _stream = new MemoryStream();
            _stream.WriteByte(0xAA);
            _stream.Seek(0, SeekOrigin.Begin);

            var fileInfoMock = new Mock<IFileInfo>();
            fileInfoMock.Setup(i => i.CreateReadStream()).Returns(_stream);
            fileInfoMock.Setup(i => i.PhysicalPath).Returns("./temp");
            _fileInfo = fileInfoMock.Object;
        }

        public void Dispose()
        {
            _stream.Close();
        }

        [Fact]
        public void PhysicalFileStreamProviderCreatesFileStreamFromFileInfo()
        {
            var provider = new PhysicalFileStreamProvider(_fileInfo);

            Assert.NotSame(_stream, provider.FileStream);
            provider.FileStream.Seek(0, SeekOrigin.Begin);
            Assert.Equal(0xAA, provider.FileStream.ReadByte());

            provider.Dispose();
        }

        [Fact]
        public void FIleStreamProviderInitializeWithPassedStream()
        {
            var provider = new PhysicalFileStreamProvider(_fileInfo);
            Assert.Equal(0xAA, provider.FileStream.ReadByte());
        }

        [Fact]
        public void FIleStreamProviderInitializationSetsStreamPositionOnBegining()
        {
            var provider = new PhysicalFileStreamProvider(_fileInfo);
            Assert.Equal(0, provider.FileStream.Position);
        }

        [Fact]
        public async void PhysicalFileStreamProviderSaveStreamToFile()
        {
            try
            {
                var provider = new PhysicalFileStreamProvider(_fileInfo);
                await provider.SaveStreamAsync();
                Assert.True(File.Exists(_fileInfo.PhysicalPath));
                _stream.Close();
                using (var fileStream = File.Open(_fileInfo.PhysicalPath, FileMode.Open))
                {
                    Assert.Equal(0xAA, fileStream.ReadByte());
                    fileStream.Close();
                    provider.Dispose();
                }
            }
            finally
            {
                if (File.Exists(_fileInfo.PhysicalPath))
                {
                    File.Delete(_fileInfo.PhysicalPath);
                }
            }
        }

        [Fact]
        public async void CanSaveStreamToFileWhenStreamOnEnd()
        {
            try
            {
                var provider = new PhysicalFileStreamProvider(_fileInfo);
                provider.FileStream.ReadByte();

                await provider.SaveStreamAsync();
                _stream.Close();

                using (var fileStream = File.Open(_fileInfo.PhysicalPath, FileMode.Open))
                {
                    Assert.Equal(0xAA, fileStream.ReadByte());
                }
            }
            finally
            {
                if (File.Exists(_fileInfo.PhysicalPath))
                {
                    File.Delete(_fileInfo.PhysicalPath);
                }
            }
        }
    }
}
