﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public class PhysicalFileStreamProvider : IFileStreamProvider, IDisposable
    {
        private IFileInfo _fileInfo;
        public Stream FileStream { get; private set; }

        public PhysicalFileStreamProvider(IFileInfo fileInfo)
        {
            _fileInfo = fileInfo;
            FileStream = new MemoryStream();

            using (var stream = fileInfo.CreateReadStream())
            {
                stream.CopyTo(FileStream);
                FileStream.Position = 0;
            }
        }

        public void Dispose()
        {
            FileStream?.Close();
        }

        public async Task SaveStreamAsync()
        {
            using (var stream = File.OpenWrite(_fileInfo.PhysicalPath))
            {
                FileStream.Seek(0, SeekOrigin.Begin);
                await FileStream.CopyToAsync(stream);
            }
        }
    }
}
