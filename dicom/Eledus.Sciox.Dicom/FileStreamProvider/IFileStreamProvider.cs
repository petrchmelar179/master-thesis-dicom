﻿using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public interface IFileStreamProvider
    {
        Stream FileStream { get; }
        Task SaveStreamAsync(); 
    }
}
