﻿using Dicom;
using System;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public class FoDicomModel : IDicomModel<IDicomDatasetWrapper>
    {
        public IImagePixelModule<IDicomDatasetWrapper> ImagePixelModule { get; }
        public uint Exposure { get; set; } = 0;

        public FoDicomModel(IImagePixelModule<IDicomDatasetWrapper> imagePixelModule)
        {
            ImagePixelModule = imagePixelModule;
        }

        public FoDicomModel() : this(new FoDicomImagePixelModule())
        {

        }

        public void Update(IDicomDatasetWrapper dataset)
        {
            Exposure = dataset.Get(DicomTag.ExposureTime, (uint)0);
            ImagePixelModule.Update(dataset);
        }

        public IDicomDatasetWrapper GetDataset()
        {
            var defaultDataset = GenerateRequiredItemsIfNotExists();
            defaultDataset.AddOrUpdate(ImagePixelModule.GetDataset());
            defaultDataset.AddOrUpdate(DicomTag.ExposureTime, Exposure.ToString());
            return defaultDataset;
        }

        private IDicomDatasetWrapper GenerateRequiredItemsIfNotExists()
        {
            var dataset = new DicomDatasetProxy();
            // type 1 atributtes
            dataset.AddOrUpdate(DicomTag.SOPClassUID, DicomUID.ComputedRadiographyImageStorage);
            dataset.AddOrUpdate(DicomTag.StudyInstanceUID, DicomUIDGenerator.GenerateDerivedFromUUID());
            dataset.AddOrUpdate(DicomTag.SeriesInstanceUID, DicomUIDGenerator.GenerateDerivedFromUUID());
            dataset.AddOrUpdate(DicomTag.SOPInstanceUID, DicomUIDGenerator.GenerateDerivedFromUUID());

            //type 2 attributes
            dataset.AddOrUpdate(DicomTag.PatientID, "12345");
            dataset.AddOrUpdate(DicomTag.PatientName, string.Empty);
            dataset.AddOrUpdate(DicomTag.PatientBirthDate, "00000000");
            dataset.AddOrUpdate(DicomTag.PatientSex, "M");
            dataset.AddOrUpdate(DicomTag.StudyDate, DateTime.Now);
            dataset.AddOrUpdate(DicomTag.StudyTime, DateTime.Now);
            dataset.AddOrUpdate(DicomTag.AccessionNumber, string.Empty);
            dataset.AddOrUpdate(DicomTag.ReferringPhysicianName, string.Empty);
            dataset.AddOrUpdate(DicomTag.StudyID, "1");
            dataset.AddOrUpdate(DicomTag.SeriesNumber, "1");
            dataset.AddOrUpdate(DicomTag.ModalitiesInStudy, "CR");
            dataset.AddOrUpdate(DicomTag.Modality, "CR");
            dataset.AddOrUpdate(DicomTag.NumberOfStudyRelatedInstances, "1");
            dataset.AddOrUpdate(DicomTag.NumberOfStudyRelatedSeries, "1");
            dataset.AddOrUpdate(DicomTag.NumberOfSeriesRelatedInstances, "1");
            dataset.AddOrUpdate(DicomTag.PatientOrientation, "F/A");
            dataset.AddOrUpdate(DicomTag.ImageLaterality, "U");

            return dataset;
        }
    }
}
