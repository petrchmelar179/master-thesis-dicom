﻿using Dicom;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public interface IDicomFileWrapper
    {
        Task SaveAsync(IFileStreamProvider stream, IDicomDatasetWrapper dataset);
        Task<IDicomDatasetWrapper> OpenAsync(IFileStreamProvider stream);
    }
}
