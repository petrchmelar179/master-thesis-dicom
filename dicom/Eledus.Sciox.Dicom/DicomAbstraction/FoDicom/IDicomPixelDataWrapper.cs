﻿namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public interface IDicomPixelDataWrapper
    {
        int NumberOfFrames { get; }
        IDicomDatasetWrapper Dataset { get; }
        void Create(IDicomDatasetWrapper dataset, bool newPixelData = false);
        byte[] GetFrame(int frame);
        void AddFrame(byte[] data);
    }
}
