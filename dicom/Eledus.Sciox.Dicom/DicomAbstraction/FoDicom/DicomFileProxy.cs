﻿using Dicom;
using Dicom.IO.Reader;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public class DicomFileProxy : IDicomFileWrapper
    {
        public async Task SaveAsync(IFileStreamProvider stream, IDicomDatasetWrapper dataset)
        {
            var foDataset = new DicomDataset(dataset);
            var foFile = new DicomFile(foDataset);
            await foFile.SaveAsync(stream.FileStream);
        }

        public async Task<IDicomDatasetWrapper> OpenAsync(IFileStreamProvider stream)
        {
            DicomFile foFile;
            try
            {
                foFile = await DicomFile.OpenAsync(stream.FileStream);
            }
            catch (DicomReaderException)
            {
                throw new InvalidDataException($"stream can not be loaded as a DICOM file");
            }
            catch (DicomFileException)
            {
                throw new InvalidDataException($"stream can not be loaded as a DICOM file");
            }

            if (foFile == null)
            {
                throw new InvalidDataException($"stream can not be loaded as a DICOM file");
            }
            return new DicomDatasetProxy(foFile.Dataset);
        }
    }
}
