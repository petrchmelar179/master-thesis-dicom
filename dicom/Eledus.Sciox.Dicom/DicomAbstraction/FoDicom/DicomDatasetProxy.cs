﻿using Dicom;
using System.Collections;
using System.Collections.Generic;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public class DicomDatasetProxy : IDicomDatasetWrapper
    {
        public DicomDataset ProxyObject { get; }
        public DicomDatasetProxy(DicomDataset dataset)
        {
            ProxyObject = dataset;
        }

        public DicomDatasetProxy() : this(new DicomDataset())
        {
            
        }

        public IDicomDatasetWrapper AddOrUpdate<T>(DicomTag tag, params T[] values)
        {
            ProxyObject.AddOrUpdate(tag, values);
            return this;
        }

        public IDicomDatasetWrapper AddOrUpdate(IEnumerable<DicomItem> items)
        {
            ProxyObject.AddOrUpdate(items);
            return this;
        }

        public bool Contains(DicomTag tag)
        {
            return ProxyObject.Contains(tag);
        }

        public T Get<T>(DicomTag tag, T defaultValue)
        {
            return ProxyObject.Get(tag, defaultValue);
        }

        public IDicomDatasetWrapper Clone()
        {
            return new DicomDatasetProxy(ProxyObject.Clone());
        }

        public IEnumerator<DicomItem> GetEnumerator()
        {
            return ProxyObject.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ProxyObject.GetEnumerator();
        }

        public IDicomDatasetWrapper AddOrUpdate(params DicomItem[] items)
        {
            ProxyObject.AddOrUpdate(items);
            return this;
        }

        public IDicomDatasetWrapper Remove(DicomTag dicomTag)
        {
            ProxyObject.Remove(dicomTag);
            return this;
        }
    }
}
