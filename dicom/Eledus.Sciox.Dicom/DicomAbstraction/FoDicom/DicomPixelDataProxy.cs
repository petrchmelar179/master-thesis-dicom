﻿using Dicom;
using Dicom.Imaging;
using Dicom.IO.Buffer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    class DicomPixelDataProxy : IDicomPixelDataWrapper
    {
        public DicomPixelData ProxyObject { get; private set; }
        public int NumberOfFrames => ProxyObject.NumberOfFrames;
        public IDicomDatasetWrapper Dataset { get; private set; }

        public DicomPixelDataProxy(IDicomDatasetWrapper dataset)
        {
            ProxyObject = DicomPixelData.Create(new DicomDataset(dataset), true);
        }

        public DicomPixelDataProxy() : this(new DicomDatasetProxy())
        {

        }

        public void Create(IDicomDatasetWrapper dataset, bool newPixelData = false)
        {
            ProxyObject = DicomPixelData.Create(new DicomDataset(dataset), newPixelData);
            Dataset = new DicomDatasetProxy(ProxyObject.Dataset);
        }

        public byte[] GetFrame(int frame)
        {
            return ProxyObject.GetFrame(frame).Data;
        }

        public void AddFrame(byte[] data)
        {
            ProxyObject.AddFrame(new MemoryByteBuffer(data));
            Dataset = new DicomDatasetProxy(ProxyObject.Dataset);
        }
    }
}
