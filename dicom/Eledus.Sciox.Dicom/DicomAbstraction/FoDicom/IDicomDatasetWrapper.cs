﻿using Dicom;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public interface IDicomDatasetWrapper : IEnumerable<DicomItem>, IEnumerable
    {
        T Get<T>(DicomTag tag, T defaultValue);
        bool Contains(DicomTag tag);
        IDicomDatasetWrapper AddOrUpdate<T>(DicomTag tag, params T[] values);
        IDicomDatasetWrapper AddOrUpdate(IEnumerable<DicomItem> items);
        IDicomDatasetWrapper AddOrUpdate(params DicomItem[] items);
        IDicomDatasetWrapper Remove(DicomTag dicomTag);
        IDicomDatasetWrapper Clone();
    }
}
