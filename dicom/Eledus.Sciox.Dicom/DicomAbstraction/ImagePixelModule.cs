﻿using Dicom;
using Dicom.Imaging;
using Dicom.IO.Buffer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public class FoDicomImagePixelModule : IImagePixelModule<IDicomDatasetWrapper>
    {
        private static readonly Dictionary<PhotometricInterpretationType, PhotometricInterpretation> _photometricTypes
            = new Dictionary<PhotometricInterpretationType, PhotometricInterpretation>
            {
                { PhotometricInterpretationType.Monochrome1, PhotometricInterpretation.Monochrome1 },
                { PhotometricInterpretationType.Monochrome2, PhotometricInterpretation.Monochrome2 },
                { PhotometricInterpretationType.PaletteColor, PhotometricInterpretation.PaletteColor },
                { PhotometricInterpretationType.Rgb, PhotometricInterpretation.Rgb },
                { PhotometricInterpretationType.YbrFull, PhotometricInterpretation.YbrFull }
            };

        private IDicomPixelDataWrapper _pixelData;

        public IEnumerable<DicomTag> RequiredTags = new List<DicomTag>()
        {
            DicomTag.SamplesPerPixel,
            DicomTag.PhotometricInterpretation,
            DicomTag.Rows,
            DicomTag.Columns,
            DicomTag.BitsAllocated,
            DicomTag.BitsStored,
            DicomTag.HighBit,
            DicomTag.PixelRepresentation,
            DicomTag.PixelData,
        };

        public ushort SamplesPerPixel { get; set; }
        public PhotometricInterpretationType PhotometricType { get; set; }
        public ushort Rows { get; set; }
        public ushort Columns { get; set; }
        public ushort BitsAllocated { get; set; }
        public ushort BitsStored { get; set; }
        public ushort HighBit { get; set; }
        public ushort PixelRepresentation { get; set; }
        public IList<byte[]> PixelData { get; private set; }

        public FoDicomImagePixelModule(IDicomPixelDataWrapper pixelData)
        {
            PixelData = new List<byte[]>();
            _pixelData = pixelData;
        }

        public FoDicomImagePixelModule()
        {
            // create default dataset to be able to create pixel data
            var dataset = new DicomDatasetProxy();
            dataset.AddOrUpdate(DicomTag.BitsAllocated, (ushort)0);
            dataset.AddOrUpdate(DicomTag.BitsStored, (ushort)0);
            dataset.AddOrUpdate(DicomTag.Columns, (ushort)0);
            dataset.AddOrUpdate(DicomTag.HighBit, (ushort)0);
            dataset.AddOrUpdate(DicomTag.PhotometricInterpretation, PhotometricInterpretation.YbrFull.Value);
            dataset.AddOrUpdate(DicomTag.PixelData, new byte[] { });
            dataset.AddOrUpdate(DicomTag.PixelRepresentation, (ushort)0);
            dataset.AddOrUpdate(DicomTag.Rows, (ushort)0);
            dataset.AddOrUpdate(DicomTag.SamplesPerPixel, (ushort)0);
            PixelData = new List<byte[]>();
            _pixelData = new DicomPixelDataProxy(dataset);
        }

        public IDicomDatasetWrapper GetDataset()
        {
            var dataset = new DicomDatasetProxy();
            dataset.AddOrUpdate(DicomTag.SamplesPerPixel, SamplesPerPixel);
            dataset.AddOrUpdate(DicomTag.PhotometricInterpretation, _photometricTypes[PhotometricType].Value);
            dataset.AddOrUpdate(DicomTag.Rows, Rows);
            dataset.AddOrUpdate(DicomTag.Columns, Columns);
            dataset.AddOrUpdate(DicomTag.BitsAllocated, BitsAllocated);
            dataset.AddOrUpdate(DicomTag.BitsStored, BitsStored);
            dataset.AddOrUpdate(DicomTag.HighBit, HighBit);
            dataset.AddOrUpdate(DicomTag.PixelRepresentation, PixelRepresentation);
            _pixelData.Create(dataset, true);
            foreach (var frame in PixelData)
            {
                _pixelData.AddFrame(frame);
            }

            return _pixelData.Dataset;
        }

        public void Update(IDicomDatasetWrapper dataset)
        {
            var missingTags = RequiredTags.Where(tag => !dataset.Contains(tag));
            if (missingTags.Any())
            {
                throw new InvalidDataException($"dataset is missing required tags ({string.Join(",", missingTags)})");
            }

            try
            {
                var photometric = dataset.Get(DicomTag.PhotometricInterpretation, PhotometricInterpretation.Monochrome2);
                PhotometricType = _photometricTypes.ToDictionary(d => d.Value, d => d.Key)[photometric];
            }
            catch (KeyNotFoundException)
            {
                throw new InvalidDataException($"unsuported photometric representation");
            }
            
            SamplesPerPixel = dataset.Get(DicomTag.SamplesPerPixel, (ushort)0);
            Rows = dataset.Get(DicomTag.Rows, (ushort)0);
            Columns = dataset.Get(DicomTag.Columns, (ushort)0);
            BitsStored = dataset.Get(DicomTag.BitsStored, (ushort)0);
            HighBit = dataset.Get(DicomTag.HighBit, (ushort)0);
            PixelRepresentation = dataset.Get(DicomTag.PixelRepresentation, (ushort)0);

            var bitsAllocated = dataset.Get(DicomTag.BitsAllocated, (ushort)0);
            if (bitsAllocated > 16)
            {
                throw new InvalidDataException($"can not represent pixel data with Bits Allocated > 16 ({BitsAllocated})");
            }
            BitsAllocated = bitsAllocated;

            try
            {
                _pixelData.Create(dataset, false);
            }
            catch (DicomImagingException)
            {
                throw new InvalidDataException($"unsuported pixel data");
            }

            PixelData = new List<byte[]>(
                Enumerable.Range(0, _pixelData.NumberOfFrames)
                .Select(index => _pixelData.GetFrame(index)));
        }

        private IDicomDatasetWrapper GetFoDicomDataset()
        {
            var dataset = new DicomDatasetProxy(new DicomDataset());
            dataset.AddOrUpdate(DicomTag.SamplesPerPixel, SamplesPerPixel);
            dataset.AddOrUpdate(DicomTag.PhotometricInterpretation, _photometricTypes[PhotometricType].Value);
            dataset.AddOrUpdate(DicomTag.Rows, Rows);
            dataset.AddOrUpdate(DicomTag.Columns, Columns);
            dataset.AddOrUpdate(DicomTag.BitsAllocated, BitsAllocated);
            dataset.AddOrUpdate(DicomTag.BitsStored, BitsStored);
            dataset.AddOrUpdate(DicomTag.HighBit, HighBit);
            dataset.AddOrUpdate(DicomTag.PixelRepresentation, PixelRepresentation);
            return dataset;
        }

        private (T result, bool isSuccessful) TryGet<T>(DicomTag tag, IDicomDatasetWrapper dataset)
        {
            T result;
            var isSuccessful = true;
            try
            {
                result = dataset.Get(DicomTag.PixelData, default(T));
            }
            catch (InvalidCastException)
            {
                isSuccessful = false;
                result = default(T);
            }

            catch (DicomDataException)
            {
                isSuccessful = false;
                result = default(T);
            }

            return (result, isSuccessful);
        }
    }
}
