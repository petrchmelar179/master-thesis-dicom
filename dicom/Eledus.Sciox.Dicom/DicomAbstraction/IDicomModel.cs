﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public interface IDicomModel<T>
    {
        IImagePixelModule<T> ImagePixelModule { get; }
        uint Exposure { get; set; }
        void Update(T dataset);
        T GetDataset();
    }
}
