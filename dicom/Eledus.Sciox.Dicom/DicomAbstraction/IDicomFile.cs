﻿using System.Threading.Tasks;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public interface IDicomFile<T>
    {
        IDicomModel<T> Model { get; }
        Task Save(IFileStreamProvider dataset);
        Task Open(IFileStreamProvider dataset);
    }
}
