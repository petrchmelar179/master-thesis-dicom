﻿using System.Collections.Generic;

namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public interface IImagePixelModule<T>
    {
        ushort SamplesPerPixel { get; set; }
        PhotometricInterpretationType PhotometricType { get; set; }
        ushort Rows { get; set; }
        ushort Columns { get; set; }
        ushort BitsAllocated { get; set; }
        ushort BitsStored { get; set; }
        ushort HighBit { get; set; }
        ushort PixelRepresentation { get; set; }
        IList<byte[]> PixelData { get; }
        T GetDataset();
        void Update(T dataset);
    }

    public enum PhotometricInterpretationType
    {
        Monochrome1,
        Monochrome2,
        PaletteColor,
        Rgb,
        YbrFull
    }
}
