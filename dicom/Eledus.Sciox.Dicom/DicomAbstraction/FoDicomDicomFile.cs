﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Dicom;


namespace Eledus.Sciox.Dicom.DicomAbstraction
{
    public class FoDicomDicomFile : IDicomFile<IDicomDatasetWrapper>
    {
        public IFileStreamProvider FileStreamProvider { get; }
        public IDicomDatasetWrapper Dataset { get; }

        public IDicomModel<IDicomDatasetWrapper> Model { get; }
        private IDicomFileWrapper _dicomFile;

        public FoDicomDicomFile(IDicomModel<IDicomDatasetWrapper> model, IDicomFileWrapper dicomFile)
        {
            Model = model;
            _dicomFile = dicomFile;
        }

        public async Task Open(IFileStreamProvider streamProvider)
        {
            var dataset = await _dicomFile.OpenAsync(streamProvider);
            Model.Update(dataset);
        }

        public async Task Save(IFileStreamProvider streamProvider)
        {
            var dataset = Model.GetDataset();
            await _dicomFile.SaveAsync(streamProvider, dataset);
        }
    }
}
