﻿setlocal

set TOOLS_PATH=%USERPROFILE%\.nuget\packages\grpc.tools\1.10.0\tools\windows_x64

cd /d %~dp0
cd ..\..\
%TOOLS_PATH%\protoc.exe -I.\ --csharp_out dicom\Eledus.Sciox.Dicom.ImprocService improc\service\grpc_service\improc_service.proto --grpc_out dicom\Eledus.Sciox.Dicom.ImprocService --plugin=protoc-gen-grpc=%TOOLS_PATH%\grpc_csharp_plugin.exe
cd /d %~dp0
endlocal