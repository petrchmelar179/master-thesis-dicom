﻿using CommandLine;
using Eledus.Sciox.Dicom.DicomAbstraction;
using Eledus.Sciox.Robox.Improc.Grpc;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Grpc.Core;
using Microsoft.Extensions.FileProviders;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Eledus.Sciox.Dicom.SampleApp
{
    class GrpcOptions
    {
        [Option('a', "address", HelpText = "Image procesing service IP address.",
            MetaValue = "127.0.0.1")]
        public string IpAddress { get; set; } = "127.0.0.1";
        [Option('p', "port", HelpText = "Image procesing service port.",
            MetaValue = "127.0.0.1")]
        public uint Port { get; set; } = 50052;
    }

    [Verb("aar", HelpText = "Apply averaging after registration on dicom files series.")]
    class AarOptions : GrpcOptions
    {
        [Option('i', "input", HelpText = "Folder of dicom files of noised images.", 
            Required = true, MetaValue = "home/noised_data_folder")]
        public string InputFolder { get; set; }

        [Option('o', "output", HelpText = "Averaged dicom output file path.", 
            Required = true, MetaValue = "result.dcm")]
        public string OutputFile { get; set; }
    }

    [Verb("hdrcalib", HelpText = "Calibrate HDR service with data.")]
    class HdrCalibOptions : GrpcOptions
    {
        [Option('i', "input", HelpText = "Folder of dicom files of calibration images.",
            Required = true, MetaValue = "home/calib_data_folder")]
        public string InputFolder { get; set; }
    }

    [Verb("hdr", HelpText = "Apply HDR service on dicom files series.")]
    class HdrOptions : GrpcOptions
    {
        [Option('i', "input", HelpText = "Folder of dicom files of LDR images.",
            Required = true, MetaValue = "home/ldr_data_folder")]
        public string InputFolder { get; set; }

        [Option('o', "output", HelpText = "Tone mapped HDR dicom output file path.",
            Required = true, MetaValue = "result.dcm")]
        public string OutputFile { get; set; }
    }

    class Program
    {
        public static Task<int> Main(string[] args)
        {
            // initialize grpc client and connection to server
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.File("sampleapp.log")
                .CreateLogger();

            return Parser.Default.ParseArguments<AarOptions, HdrCalibOptions, HdrOptions>(args).MapResult(
                async (AarOptions opts) => await ApplyAar(opts),
                async (HdrCalibOptions opts) => await HdrCalib(opts),
                async (HdrOptions opts) => await Hdr(opts),
                errs => Task.FromResult(0));
        }

        public static Improc.ImprocClient CreateClient(GrpcOptions options)
        {
            var channel = new Channel($"{options.IpAddress}:{options.Port}", ChannelCredentials.Insecure);
            var client = new Improc.ImprocClient(channel);
            Log.Information("GRPC client initalized.");
            return client;
        }

        public static async Task<int> Hdr(HdrOptions options)
        {
            var client = CreateClient(options);
            Log.Information("hdr parameters succesfully parsed.");
            if (!Directory.Exists(options.InputFolder))
            {
                Log.Error($"Directory {options.InputFolder} doesn't exist.");
                return 1;
            }

            var dicomPaths = Directory.GetFiles(options.InputFolder, "*.dcm");

            if (dicomPaths.Count() < 2)
            {
                Log.Error($"Input folder has to contain more than 1 dicom files to process HDR");
                return 1;
            }

            IEnumerable<IDicomFile<IDicomDatasetWrapper>> dicoms;
            try
            {
                dicoms = await LoadDicomFiles(dicomPaths);
            }
            catch (FileNotFoundException ex)
            {
                Log.Error($"Dicom files loading failed with error message: {ex.Message}");
                return 1;
            }

            HdrResponse response;
            try
            {
                var request = new HdrRequest()
                {
                    LdrPixelData = { dicoms.Select(dicom => dicom.Model.ImagePixelModuleToPixelData()) }
                };
                Log.Information($"Calling HDR request on improc service.");
                response = await client.HdrAsync(request);
                Log.Information($"HDR request completed with succesfull response.");
            }
            catch (RpcException ex)
            {
                Log.Error($"Rpc call failed: {ex.Message}");
                return 1;
            }

            Log.Information($"Creating new DICOM data from HDR response.");
            var newModel = response.HdrPixelData.PixelDataToImagePixelModule();
            var newDicomFile = dicoms.First();
            newDicomFile.Model.Update(newModel.GetDataset());
            Log.Information($"New DICOM data was succesfully created from HDR response.");

            Log.Information($"Creating new file: {options.OutputFile}.");
            var fileInfo = new FileInfo(options.OutputFile);
            fileInfo.Create().Close();
            Log.Information($"New file {options.OutputFile} was succesfully created.");

            Log.Information($"Writing new DICOM data to {options.OutputFile}.");
            var physicFileProvider = new PhysicalFileProvider(fileInfo.DirectoryName);
            var fileProvider = new PhysicalFileStreamProvider(physicFileProvider.GetFileInfo(fileInfo.Name));
            await dicoms.First().Save(fileProvider);
            await fileProvider.SaveStreamAsync();
            Log.Information($"New DICOM data was succesfully written to {options.OutputFile}.");
            return 0;
        }

        public static async Task<int> HdrCalib(HdrCalibOptions options)
        {
            var client = CreateClient(options);
            Log.Information("hdrcalib parameters succesfully parsed.");
            if (!Directory.Exists(options.InputFolder))
            {
                Log.Error($"Directory {options.InputFolder} doesn't exist.");
                return 1;
            }

            var dicomPaths = Directory.GetFiles(options.InputFolder, "*.dcm");

            if (dicomPaths.Count() < 2)
            {
                Log.Error($"Input folder has to contain more than 1 dicom files to process HDR calibration");
                return 1;
            }

            IEnumerable<IDicomFile<IDicomDatasetWrapper>> dicoms;
            try
            {
                dicoms = await LoadDicomFiles(dicomPaths);
            }
            catch (FileNotFoundException ex)
            {
                Log.Error($"Dicom files loading failed with error message: {ex.Message}");
                return 1;
            }

            HdrCalibrationResponse response;
            try
            {
                var request = new HdrCalibrationRequest()
                {
                    LdrPixelData = { dicoms.Select(dicom => dicom.Model.ImagePixelModuleToPixelData()) }
                };
                Log.Information($"Calling HDR calibration request on improc service.");
                response = await client.HdrCalibrationAsync(request);
                Log.Information($"HDR calibration request completed with succesfull response.");
            }
            catch (RpcException ex)
            {
                Log.Error($"Rpc call failed: {ex.Message}");
                return 1;
            }

            Log.Information($"HDR calibration succesfully done.");
            return 0;
        }

        public static async Task<int> ApplyAar(AarOptions options)
        {
            var client = CreateClient(options);

            Log.Information("aar parameters succesfully parsed.");
            if (!Directory.Exists(options.InputFolder))
            {
                Log.Error($"Directory {options.InputFolder} doesn't exist.");
                return 1;
            }

            var dicomPaths = Directory.GetFiles(options.InputFolder, "*.dcm");

            if (dicomPaths.Count() < 2)
            {
                Log.Error($"Input folder has to contain more than 1 dicom files to process AAR");
                return 1;
            }

            IEnumerable<IDicomFile<IDicomDatasetWrapper>> dicoms;
            try
            {
                dicoms = await LoadDicomFiles(dicomPaths);
            }
            catch (FileNotFoundException ex)
            {
                Log.Error($"Dicom files loading failed with error message: {ex.Message}");
                return 1;
            }

            AarResponse response;
            try
            {
                var request = new AarRequest()
                {
                    AarPixelData = { dicoms.Select(dicom => dicom.Model.ImagePixelModuleToPixelData()) }
                };
                Log.Information($"Calling AAR request on improc service.");
                response = await client.AarAsync(request);
                Log.Information($"AAR request completed with succesfull response.");
            }
            catch (RpcException ex)
            {
                Log.Error($"Rpc call failed: {ex.Message}");
                return 1;
            }

            Log.Information($"Creating new DICOM data from AAR response.");
            var newModel = response.AarPixelData.PixelDataToImagePixelModule();
            var newDicomFile = dicoms.First();
            newDicomFile.Model.Update(newModel.GetDataset());
            Log.Information($"New DICOM data was succesfully created from AAR response.");

            Log.Information($"Creating new file: {options.OutputFile}.");
            var fileInfo = new FileInfo(options.OutputFile);
            fileInfo.Create().Close();
            Log.Information($"New file {options.OutputFile} was succesfully created.");

            Log.Information($"Writing new DICOM data to {options.OutputFile}.");
            var physicFileProvider = new PhysicalFileProvider(fileInfo.DirectoryName);
            var fileProvider = new PhysicalFileStreamProvider(physicFileProvider.GetFileInfo(fileInfo.Name));
            await dicoms.First().Save(fileProvider);
            await fileProvider.SaveStreamAsync();
            Log.Information($"New DICOM data was succesfully written to {options.OutputFile}.");
            return 0;
        }

        public static async Task<IEnumerable<IDicomFile<IDicomDatasetWrapper>>> LoadDicomFiles(params string[] dicomFilesPaths)
        {
            var dicoms = new List<IDicomFile<IDicomDatasetWrapper>>();
            foreach (var path in dicomFilesPaths)
            {
                Log.Information($"Loading {path}");
                var fileInfo = new FileInfo(path);
                var physicFileProvider = new PhysicalFileProvider(fileInfo.DirectoryName);
                var fileProvider = new PhysicalFileStreamProvider(physicFileProvider.GetFileInfo(fileInfo.Name));
                var dicomFile = new FoDicomDicomFile(new FoDicomModel(), new DicomFileProxy());

                await dicomFile.Open(fileProvider);
                Log.Information($"File {path} was succesfully loaded");
                dicoms.Add(dicomFile);
            }
            return dicoms;
        }
    }

    public static class ExtensionMethods
    {
        public static PixelData ImagePixelModuleToPixelData(this IDicomModel<IDicomDatasetWrapper> model)
        {
            var imagePixelModule = model.ImagePixelModule;
            if (imagePixelModule.PixelData.Count != 1)
            {
                throw new InvalidDataException("multiframe dicom images are not supported");
            }

            return new PixelData()
            {
                BitsAllocated = imagePixelModule.BitsAllocated,
                BitsStored = imagePixelModule.BitsStored,
                Columns = imagePixelModule.Columns,
                Rows = imagePixelModule.Rows,
                ExposureMs = model.Exposure,
                PixelData_ = ByteString.CopyFrom(imagePixelModule.PixelData[0])
            };
        }

        public static IDicomModel<IDicomDatasetWrapper> PixelDataToImagePixelModule(this PixelData pixelData)
        {
            var imagePixelModule = new FoDicomImagePixelModule()
            {
                BitsStored = (ushort)pixelData.BitsStored,
                BitsAllocated = (ushort)pixelData.BitsAllocated,
                HighBit = (ushort)(pixelData.BitsStored - 1),
                PhotometricType = PhotometricInterpretationType.Monochrome2,
                Columns = (ushort)pixelData.Columns,
                Rows = (ushort)pixelData.Rows,
                PixelRepresentation = 0,
                SamplesPerPixel = 1,
                PixelData = { pixelData.PixelData_.ToArray() }
            };

            return new FoDicomModel(imagePixelModule)
            {
                Exposure = pixelData.ExposureMs
            };
        }
    }
}
