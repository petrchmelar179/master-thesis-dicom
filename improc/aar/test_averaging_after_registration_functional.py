import pytest
from glob import glob
from os.path import join
import numpy as np

from skimage.io import imread
from skimage.transform import rotate, downscale_local_mean
from aar.averaging_after_registration import detect_key_points, align_images


@pytest.fixture()
def noised_images(request):
    images = []
    for path in glob(join('resources', 'noised_images', '*')):
        image = downscale_local_mean(imread(path), (2, 2)).astype(np.uint16)
        images.append(image)
    return np.array(images)


def test_aar_align_images_correctly(noised_images):
    key_points, descriptors = detect_key_points(noised_images)
    aligned_images = align_images(noised_images, key_points, descriptors)
    np.testing.assert_equal(aligned_images.shape, noised_images.shape)


def test_detect_ley_points_detects_key_points_on_random_data():
    image = np.random.rand(100, 100) * 255
    images = np.array([image, rotate(image, 90)])
    key_points, descriptors = detect_key_points(images)
    aligned_images = align_images(images, key_points, descriptors)

    np.testing.assert_equal(aligned_images.shape, images.shape)

