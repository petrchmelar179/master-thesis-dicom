import pytest
import numpy as np
from matplotlib import pyplot as plt

from skimage.data import camera
from skimage.transform import downscale_local_mean, rotate

from aar.averaging_after_registration import align_images, detect_key_points, align_images


@pytest.fixture()
def images_with_key_points(request):
    camera_image = downscale_local_mean(camera(), (4, 4))
    images = np.array([camera_image, camera_image])
    return images


@pytest.fixture()
def images_key_points_descriptors_set(request):
    # create image with one straight line and its rotated equivalent
    # after that setup key points and its binary descriptors for alignment algorithm so it will be able to rotate
    # second image back
    image1 = np.zeros((9, 9))
    image1[4, :] = 255
    image2 = rotate(image1, 90)

    # setup one key point to the roation center and second to in the way it will be rotated for second image key points
    key_points_1 = np.array([[4.121, 4.1212], [4.134, 5.143], [4.1323, 6.233], [4.32, 7.23]], dtype=np.float)
    key_points_2 = np.array([[4.1224, 5.2332], [4.123, 6.023], [4.1232, 7.1233], [4.1, 8.23]], )
    descriptors1 = np.array([[True, True], [True, False], [False, True], [False, False]], dtype=np.bool)
    descriptors2 = np.array([[True, True], [True, False], [False, True], [False, False]], dtype=np.bool)
    return np.array([image1, image2], dtype=np.uint8),\
           np.array([key_points_1, key_points_2]),\
           np.array([descriptors1, descriptors2])


def test_align_images_exists(images_key_points_descriptors_set):
    images, key_points, descriptors = images_key_points_descriptors_set
    aligned_images = align_images(images, key_points, descriptors)
    assert images.shape == aligned_images.shape


def test_align_images_returns_array_with_same_shape_as_passed_images(images_key_points_descriptors_set):
    images, key_points, descriptors = images_key_points_descriptors_set
    aligned_images = align_images(images, key_points, descriptors)
    np.testing.assert_equal(aligned_images.shape, images.shape)


def test_align_images_raises_value_error_when_images_has_incorrect_dimensions():
    with pytest.raises(ValueError):
        align_images(np.zeros((10, 10)), np.zeros((2, 10, 2)), np.zeros((2, 10, 10)))


def test_align_images__raises_value_error_when_key_points_has_wrong_dimensions():
    with pytest.raises(ValueError):
        align_images(np.zeros((10, 10, 10)), np.zeros((2, 10)), np.zeros((2, 10, 10)))


def test_align_images__raises_value_error_when_key_point_has_wrong_dimensions():
    with pytest.raises(ValueError):
        align_images(np.zeros((10, 10, 10)), np.zeros((2, 10, 10)), np.zeros((2, 10, 10)))


def test_align_images__raises_value_error_when_descriptors_has_wrong_dimensions():
    with pytest.raises(ValueError):
        align_images(np.zeros((10, 10, 10)), np.zeros((2, 10, 2)), np.zeros((2, 10)))


def test_align_images_raises_value_error_when_descriptors_count_not_equal_to_key_points_count():
    with pytest.raises(ValueError):
        align_images(np.zeros((10, 10, 10)), np.zeros((2, 10, 2)), np.zeros((10, 10, 10)))


def test_detect_key_points_exists(images_with_key_points):
    detect_key_points(images_with_key_points)


def test_detect_key_points_raises_value_error_when_images_has_incorrect_dimensions():
    with pytest.raises(ValueError):
        detect_key_points(np.zeros((10, 10)))


def test_detect_key_points_returns_numpy_array_of_key_points(images_with_key_points):
    key_points, _ = detect_key_points(images_with_key_points)
    assert type(key_points) is np.ndarray


def test_detect_key_points_returns_numpy_array_of_descriptors(images_with_key_points):
    _, descriptors = detect_key_points(images_with_key_points)
    assert type(descriptors) is np.ndarray


def test_detect_key_points_returns_numpy_array_of_key_points_with_correct_dimensions(images_with_key_points):
    key_points, _ = detect_key_points(images_with_key_points)
    np.testing.assert_equal(key_points.shape[2::], (2,))


def test_detect_key_points_returns_correct_number_of_key_point_sets(images_with_key_points):
    key_points, _ = detect_key_points(images_with_key_points)
    assert key_points.shape[0] == images_with_key_points.shape[0]


def test_detect_key_points_returns_bool_numpy_array_of_descriptors(images_with_key_points):
    _, descriptors = detect_key_points(images_with_key_points)
    assert descriptors.dtype == 'bool'


def test_detect_key_points_returns_bool_numpy_array_of_descriptors_with_correct_dimensions(images_with_key_points):
    _, descriptors = detect_key_points(images_with_key_points)
    assert len(descriptors.shape) == 3


def test_detect_key_points_returns_correct_number_of_descriptor_sets(images_with_key_points):
    _, descriptors = detect_key_points(images_with_key_points)
    assert descriptors.shape[0] == images_with_key_points.shape[0]


def test_detect_key_points_raises_value_error_when_no_key_points_in_images():
    with pytest.raises(ValueError):
        detect_key_points(np.zeros((10, 10, 10)))
