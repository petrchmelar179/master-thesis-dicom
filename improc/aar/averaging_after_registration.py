import numpy as np
from skimage.feature import ORB, match_descriptors
from skimage.transform import ProjectiveTransform, warp
from skimage.exposure import rescale_intensity
from skimage.measure import ransac

import matplotlib.pyplot as plt


def align_images(images, key_points, descriptors):
    if len(images.shape) != 3:
        raise ValueError(f"invalid images dimensions. Expected images array with shape (c ,m, n) "
                         f"where c is number of images, m number of rows and n number of columns."
                         f"Current shape is: {images.shape}")

    if len(key_points.shape) != 3:
        raise ValueError("invalid key points data. Data with (n, a, 2) shape are required for n is number of images"
                         "from which has been key points extracted and a is number of key points per image")

    if key_points.shape[2] != 2:
        raise ValueError("invalid key points data. Data with (n, a, 2) shape are required for n is number of images"
                         "from which has been key points extracted and a is number of key points per image")

    if len(descriptors.shape) != 3:
        raise ValueError("invalid key points data. Data with (n, a, 2) shape are required for n is number of images"
                         "from which has been key points extracted and a is number of key points per image")

    if descriptors.shape[0] != key_points.shape[0]:
        raise ValueError("number of descriptors has to be equal to number of key points")

    aligned_images = np.zeros_like(images)
    aligned_images[0, :, :] = images[0]

    for idx in range(1, images.shape[0]):
        matches_idx = match_descriptors(descriptors[0], descriptors[idx])

        # [:, ::-1] is used for swapping of axes
        src = key_points[idx, matches_idx[:, 1]][:, ::-1]
        dst = key_points[0, matches_idx[:, 0]][:, ::-1]

        model_robust, _ = ransac((src, dst),
                                 ProjectiveTransform,
                                 min_samples=4,
                                 residual_threshold=2)

        # warp image
        warped_image = warp(images[idx], model_robust.inverse, mode='edge', preserve_range=True)
        aligned_images[idx, :, :] = warped_image
    return aligned_images


def detect_key_points(images):
    if len(images.shape) != 3:
        raise ValueError(f"invalid images dimensions. Expected images array with shape (c ,m, n) "
                         f"where c is number of images, m number of rows and n number of columns."
                         f"Current shape is: {images.shape}")

    orb = ORB(n_keypoints=200, fast_threshold=0.04)
    try:
        descriptors = []
        key_points = []
        for image in images:
            orb.detect_and_extract(image)
            descriptors.append(orb.descriptors)
            key_points.append(orb.keypoints)

    except RuntimeError:
        raise ValueError("invalid images data. No key points found.")

    return np.array(key_points), np.array(descriptors)

