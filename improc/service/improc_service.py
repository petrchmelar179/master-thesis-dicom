from service.grpc_service.improc_service_pb2_grpc import ImprocServicer, add_ImprocServicer_to_server
from service.grpc_service.improc_service_pb2 import PixelData, HdrCalibrationResponse, HdrResponse, AarResponse

import numpy as np
import grpc
from concurrent import futures
import matplotlib.pyplot as plt

from skimage.exposure import rescale_intensity
from skimage.transform import downscale_local_mean

from hdr.debevec_hdr import detector_response_solver, construct_radiance_map
from hdr.drago_tone_mapping import apply_drago_tone_mapping
from aar.averaging_after_registration import detect_key_points, align_images
import cv2


class ConcreteImprocServicer(ImprocServicer):
    g = []

    def __init__(self,
                 pixel_data_to_numpy_array_func,
                 numpy_array_to_pixel_data_func,
                 apply_hdr_func,
                 apply_aar_func,
                 calibrate_hdr_func):
        super(ConcreteImprocServicer, self).__init__()
        self.pixel_data_to_numpy_array = pixel_data_to_numpy_array_func
        self.numpy_array_to_pixel_data = numpy_array_to_pixel_data_func
        self.apply_hdr = apply_hdr_func
        self.apply_aar = apply_aar_func
        self.calibrate_hdr = calibrate_hdr_func

    def HdrCalibration(self, request, context):
        self._hdr_request_check(request)
        ldr_images = np.array([self.pixel_data_to_numpy_array(pixel_data) for pixel_data in request.LdrPixelData])
        exposures = np.array([pixel_data.ExposureMs / 1000 for pixel_data in request.LdrPixelData])
        g = self.calibrate_hdr(ldr_images, exposures)
        self.g = g
        return HdrCalibrationResponse(g=g)

    def Hdr(self, request, context):
        self._hdr_request_check(request)

        if len(self.g) != 2 ** request.LdrPixelData[0].BitsAllocated:
            raise RuntimeError('invalid g function. Hdr is not calibrated or has been calibrated with other data')

        ldr_images = np.array([self.pixel_data_to_numpy_array(pixel_data) for pixel_data in request.LdrPixelData])
        exposures = np.array([pixel_data.ExposureMs / 1000 for pixel_data in request.LdrPixelData])

        hdr_image = self.apply_hdr(ldr_images, exposures, self.g)
        pixel_data = self.numpy_array_to_pixel_data(hdr_image, request.LdrPixelData[0].BitsStored, 0)
        return HdrResponse(HdrPixelData=pixel_data)

    def Aar(self, request, context):
        self._aar_request_check(request)

        noised_images = np.array([self.pixel_data_to_numpy_array(pixel_data) for pixel_data in request.AarPixelData])
        averaged_image = self.apply_aar(noised_images)
        pixel_data = self.numpy_array_to_pixel_data(averaged_image, request.AarPixelData[0].BitsStored, 0)
        return AarResponse(AarPixelData=pixel_data)

    @staticmethod
    def _hdr_request_check(request):
        if len(request.LdrPixelData) < 2:
            raise ValueError("number of images has to be greater than 1")

        if np.unique([pixel_data.Rows for pixel_data in request.LdrPixelData]).size != 1:
            raise ValueError('all pixel_data has to have same number of Rows')

        if np.unique([pixel_data.Columns for pixel_data in request.LdrPixelData]).size != 1:
            raise ValueError('all pixel_data has to have same number of Columns')

        if np.unique([pixel_data.BitsAllocated for pixel_data in request.LdrPixelData]).size != 1:
            raise ValueError('all pixel_data has to have same number of BitsAllocated')

        if np.unique([pixel_data.BitsStored for pixel_data in request.LdrPixelData]).size != 1:
            raise ValueError('all pixel_data has to have same number of BitsStored')

    @staticmethod
    def _aar_request_check(request):
        if len(request.AarPixelData) < 2:
            raise ValueError("number of images has to be greater than 1")

        if np.unique([pixel_data.Rows for pixel_data in request.AarPixelData]).size != 1:
            raise ValueError('all pixel_data has to have same number of Rows')

        if np.unique([pixel_data.Columns for pixel_data in request.AarPixelData]).size != 1:
            raise ValueError('all pixel_data has to have same number of Columns')

        if np.unique([pixel_data.BitsAllocated for pixel_data in request.AarPixelData]).size != 1:
            raise ValueError('all pixel_data has to have same number of BitsAllocated')

        if np.unique([pixel_data.BitsStored for pixel_data in request.AarPixelData]).size != 1:
            raise ValueError('all pixel_data has to have same number of BitsStored')


def pixel_data_to_numpy_array(pixel_data):
    if type(pixel_data) is not PixelData:
        raise ValueError('pixel_data parameter has to be of type PixelData')

    if pixel_data.Rows < 1 or pixel_data.Columns < 1:
        raise ValueError('number of Rows and Columns has to be greater than 0')

    if pixel_data.BitsStored < 1 or pixel_data.BitsStored > 16:
        raise ValueError('bits stored has to be in range from 1 to 16')

    if pixel_data.BitsStored > pixel_data.BitsAllocated:
        raise ValueError('bits stored has to be less than bits allocated')

    if pixel_data.BitsAllocated == 16:
        data_type = np.uint16
    elif pixel_data.BitsAllocated == 8:
        data_type = np.uint8
    else:
        raise ValueError('bits allocated has to be 8 or 16')

    number_of_bytes_allocated = int(pixel_data.BitsAllocated / 8)
    if len(pixel_data.PixelData) != pixel_data.Columns * pixel_data.Rows * number_of_bytes_allocated:
        raise ValueError('wrong number of bytes in pixel data')

    array = np.frombuffer(pixel_data.PixelData, dtype=data_type)
    rescaled_array = rescale_intensity(array, in_range=(0, 2**pixel_data.BitsStored - 1), out_range=data_type)\
        .astype(data_type)
    return rescaled_array.reshape(pixel_data.Rows, pixel_data.Columns)


def numpy_array_to_pixel_data(numpy_array, bits_stored, exposure_ms=0):
    if numpy_array.itemsize * 8 < bits_stored:
        raise ValueError('number of bits stored has to be less than number of bits in numpy array dtype')

    if numpy_array.dtype not in [np.dtype(np.uint8), np.dtype(np.uint16)]:
        raise ValueError('passed numpy array has to be of type uint8 or uint16')

    if len(numpy_array.shape) != 2:
        raise ValueError('passed numpy array has to have only two dimensions')

    rescaled_array = rescale_intensity(numpy_array,
                                       in_range=numpy_array.dtype.name,
                                       out_range=(0, 2**bits_stored - 1))

    return PixelData(Rows=numpy_array.shape[0],
                     Columns=numpy_array.shape[1],
                     BitsAllocated=numpy_array.dtype.itemsize * 8,
                     BitsStored=bits_stored,
                     ExposureMs=exposure_ms,
                     PixelData=rescaled_array.tobytes())


def calibrate_hdr(calibration_images, exposures):
    g, _ = detector_response_solver(calibration_images, exposures, 1)
    return g


def apply_hdr(ldr_images, exposures, g):
    radiance_map = construct_radiance_map(ldr_images, exposures, g)
    hdr_float_image = apply_drago_tone_mapping(radiance_map, 0.75)
    return rescale_intensity(hdr_float_image, in_range=hdr_float_image.dtype.name, out_range=ldr_images.dtype.name)\
        .astype(ldr_images.dtype)


def apply_aar(noised_images):
    key_points, descriptors = detect_key_points(noised_images)
    aligned_images = align_images(noised_images, key_points, descriptors)
    return np.mean(aligned_images, axis=0).astype(noised_images.dtype)


def start_server(ip, port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    improc_servicer = ConcreteImprocServicer(pixel_data_to_numpy_array_func=pixel_data_to_numpy_array,
                                             numpy_array_to_pixel_data_func=numpy_array_to_pixel_data,
                                             calibrate_hdr_func=calibrate_hdr,
                                             apply_hdr_func=apply_hdr,
                                             apply_aar_func=apply_aar)

    add_ImprocServicer_to_server(improc_servicer, server)
    server.add_insecure_port(f'{ip}:{port}')
    server.start()
    return server
