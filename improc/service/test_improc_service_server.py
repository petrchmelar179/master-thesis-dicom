from service.improc_service import start_server
from config import service_ip, service_port

from service.grpc_service import improc_service_pb2_grpc, improc_service_pb2
from service.improc_service import numpy_array_to_pixel_data, pixel_data_to_numpy_array

from skimage.transform import downscale_local_mean
from skimage.exposure import rescale_intensity

import grpc
import pytest
import numpy as np
import matplotlib.pyplot as plt

from hdr.test_hdr_functional import calibration_images, ldr_images
from aar.test_averaging_after_registration_functional import noised_images


@pytest.fixture
def server(ip=service_ip, port=service_port):
    server = start_server(ip, port)
    yield server
    server.stop(0)


@pytest.fixture
def stub(ip=service_ip, port=service_port):
    channel = grpc.insecure_channel(f'{ip}:{port}')
    return improc_service_pb2_grpc.ImprocStub(channel)


@pytest.fixture
def calibrate_hdr_response(server, stub, calibration_images):
    images, exposures = calibration_images
    pixel_data = [numpy_array_to_pixel_data(
        np.array(downscale_local_mean(calibration_image, (50, 50)), dtype=np.uint16),
        16,
        int(exposure * 1000)) for calibration_image, exposure in zip(images, exposures)]
    request = improc_service_pb2.HdrCalibrationRequest(LdrPixelData=pixel_data)
    return stub.HdrCalibration(request)


@pytest.fixture
def hdr_response(server, stub, calibrate_hdr_response, ldr_images):
    images, exposures = ldr_images
    pixel_data = [numpy_array_to_pixel_data(
        np.array(downscale_local_mean(calibration_image, (10, 10)), dtype=np.uint16),
        16,
        int(exposure * 1000)) for calibration_image, exposure in zip(images, exposures)]
    request = improc_service_pb2.HdrRequest(LdrPixelData=pixel_data)
    return stub.Hdr(request), request


@pytest.fixture
def aar_response(server, stub, noised_images):
    pixel_data = [numpy_array_to_pixel_data(
        image,
        16) for image in noised_images[:5]]
    request = improc_service_pb2.AarRequest(AarPixelData=pixel_data)
    return stub.Aar(request), request


def test_imroc_servicer_can_calibrate_hdr(calibrate_hdr_response):
    assert len(calibrate_hdr_response.g) == 2 ** 16


def test_improc_servicer_can_apply_hdr(hdr_response):
    response, request = hdr_response
    assert len(request.LdrPixelData[0].PixelData) == len(response.HdrPixelData.PixelData)


def test_improc_servicer_can_apply_aar(aar_response):
    response, request = aar_response
    assert len(request.AarPixelData[0].PixelData) == len(response.AarPixelData.PixelData)
