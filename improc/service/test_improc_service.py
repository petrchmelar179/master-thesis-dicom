from service.improc_service import pixel_data_to_numpy_array, numpy_array_to_pixel_data, apply_hdr
from service.grpc_service.improc_service_pb2 import PixelData, HdrCalibrationRequest, HdrCalibrationResponse,\
    HdrRequest, HdrResponse, AarRequest, AarResponse

from service.improc_service import ConcreteImprocServicer
import pytest
import numpy as np
from service.improc_service import start_server


@pytest.fixture()
def pixel_data(request):
    data = PixelData(Rows=1,
                     Columns=1,
                     BitsAllocated=8,
                     BitsStored=8,
                     PixelData=bytes([0]))
    return data


@pytest.fixture(params=[3, 7, 20])
def bits_allocated(request):
    return request.param


def test_pixel_data_to_numpy_array_exists(pixel_data):
    pixel_data_to_numpy_array(pixel_data)


def test_pixel_data_raises_value_error_when_data_not_grpc_pixel_data():
    with pytest.raises(ValueError):
        pixel_data_to_numpy_array('dummy')


def test_pixel_data_raises_value_error_when_rows_are_less_than_0(pixel_data):
    pixel_data.Rows = 0
    with pytest.raises(ValueError):
        pixel_data_to_numpy_array(pixel_data)


def test_pixel_data_raises_value_error_when_columns_are_less_than_0(pixel_data):
    pixel_data.Columns = 0
    with pytest.raises(ValueError):
        pixel_data_to_numpy_array(pixel_data)


def test_pixel_data_raises_value_error_when_bits_allocated_is_not_8_or_16(pixel_data, bits_allocated):
    pixel_data.BitsAllocated = bits_allocated
    pixel_data.BitsStored = 1
    with pytest.raises(ValueError):
        pixel_data_to_numpy_array(pixel_data)


def test_pixel_data_raises_value_error_when_bits_stored_is_less_then_1(pixel_data):
    pixel_data.BitsStored = 0
    with pytest.raises(ValueError):
        pixel_data_to_numpy_array(pixel_data)


def test_pixel_data_raises_value_error_when_bits_stored_is_greater_than_16(pixel_data):
    pixel_data.BitsStored = 17
    with pytest.raises(ValueError):
        pixel_data_to_numpy_array(pixel_data)


def test_pixel_data_raises_value_error_when_bits_stored_is_greater_than_bits_allocated(pixel_data):
    pixel_data.BitsStored = 16
    pixel_data.BitsAllocated = 8
    with pytest.raises(ValueError):
        pixel_data_to_numpy_array(pixel_data)


def test_pixel_data_returns_numpy_array_with_correct_shape(pixel_data):
    pixel_data.Rows = 10
    pixel_data.Columns = 2
    pixel_data.PixelData = bytes(20)
    numpy_array = pixel_data_to_numpy_array(pixel_data)
    np.testing.assert_equal(numpy_array.shape, (10, 2))


def test_pixel_data_returns_numpy_array_with_correct_data(pixel_data):
    data = [0, 1, 2, 3]
    pixel_data.Rows = 2
    pixel_data.Columns = 2
    pixel_data.PixelData = bytes(data)
    numpy_array = pixel_data_to_numpy_array(pixel_data)
    np.testing.assert_equal(numpy_array, np.array(data).reshape(2, 2))


def test_pixel_data_returns_numpy_array_with_correct_data(pixel_data):
    data = [0, 1, 2, 3]
    pixel_data.Rows = 2
    pixel_data.Columns = 2
    pixel_data.PixelData = bytes(data)
    numpy_array = pixel_data_to_numpy_array(pixel_data)
    np.testing.assert_equal(numpy_array, np.array(data).reshape(2, 2))


def test_pixel_data_returns_numpy_array_of_dtype_uin16_when_bits_allocated_is_16(pixel_data):
    pixel_data.BitsAllocated = 16
    pixel_data.PixelData = bytes(2)
    numpy_array = pixel_data_to_numpy_array(pixel_data)
    assert numpy_array.dtype == np.dtype(np.uint16)


def test_pixel_data_returns_numpy_array_of_dtype_uin8_when_bits_allocated_is_8(pixel_data):
    pixel_data.BitsAllocated = 8
    numpy_array = pixel_data_to_numpy_array(pixel_data)
    assert numpy_array.dtype == np.dtype(np.uint8)


def test_pixel_data_raises_value_error_when_not_enough_data_in_buffer(pixel_data):
    pixel_data.BitsAllocated = 16
    pixel_data.PixelData = bytes(1)
    with pytest.raises(ValueError):
        pixel_data_to_numpy_array(pixel_data)


def test_pixel_data_are_normalized_when_bits_stored_are_less_than_bits_allocated(pixel_data):
    pixel_data.PixelData = bytes([0, 1])
    pixel_data.BitsStored = 1
    pixel_data.Rows = 1
    pixel_data.Columns = 2
    pixel_array = pixel_data_to_numpy_array(pixel_data)
    np.testing.assert_equal(pixel_array, np.array([[0, 255]], dtype=np.uint8))


def test_numpy_array_to_pixel_data_exists():
    numpy_array_to_pixel_data(np.zeros((1, 1), dtype=np.uint8), 8)


def test_numpy_array_to_pixel_data_raises_value_error_when_bits_stored_is_greater_than_bits_arrays_bits_allocated():
    with pytest.raises(ValueError):
        numpy_array_to_pixel_data(np.zeros(10, dtype=np.uint8), 11)


def test_numpy_array_to_pixel_data_raises_value_error_when_not_uint16_or_uint8():
    with pytest.raises(ValueError):
        numpy_array_to_pixel_data(np.zeros(8, dtype=np.float), 8)


def test_numpy_array_to_pixel_data_raises_value_error_when_passed_array_has_wrong_dimensions():
    with pytest.raises(ValueError):
        numpy_array_to_pixel_data(np.zeros(8, dtype=np.uint8), 8)


def test_numpy_array_to_pixel_data_returns_pixel_data():
    data = numpy_array_to_pixel_data(np.zeros((1, 1), dtype=np.uint8), 8)
    assert type(data) is PixelData


def test_numpy_array_to_pixel_data_returns_pixel_data_with_correct_number_of_rows():
    data = numpy_array_to_pixel_data(np.zeros((2, 3), dtype=np.uint8), 8)
    assert data.Rows == 2


def test_numpy_array_to_pixel_data_returns_pixel_data_witch_correct_number_of_columns():
    data = numpy_array_to_pixel_data(np.zeros((2, 3), dtype=np.uint8), 8)
    assert data.Columns == 3


def test_numpy_array_to_pixel_data_returns_pixel_data_with_correct_bits_allocated():
    data = numpy_array_to_pixel_data(np.zeros((2, 3), dtype=np.uint8), 8)
    assert data.BitsAllocated == 8


def test_numpy_array_to_pixel_data_returns_pixel_data_with_correct_bits_stored():
    data = numpy_array_to_pixel_data(np.zeros((2, 3), dtype=np.uint8), 2)
    assert data.BitsStored == 2


def test_numpy_array_to_pixel_data_returns_pixel_data_with_correct_exposure_ms():
    data = numpy_array_to_pixel_data(np.zeros((2, 3), dtype=np.uint8), 2, 10)
    assert data.ExposureMs == 10


def test_numpy_array_to_pixel_data_returns_correct_pixel_data():
    data = numpy_array_to_pixel_data(np.array([[2, 3, 4], [1, 1, 1]], dtype=np.uint8), 8)
    assert bytes([2, 3, 4, 1, 1, 1]) == data.PixelData


def test_numpy_to_pixel_data_rescales_array_data_according_to_bits_stored_correctly():
    data = numpy_array_to_pixel_data(np.array([[0, 3]], dtype=np.uint8), 7)
    assert bytes([0, 1]) == data.PixelData


@pytest.fixture()
def dummy_improc_servicer():
    return ConcreteImprocServicer(
        pixel_data_to_numpy_array_func=lambda a: None,
        numpy_array_to_pixel_data_func=lambda a, b, c: None,
        calibrate_hdr_func=lambda a, b: None,
        apply_hdr_func=lambda a, b, c: None,
        apply_aar_func=lambda a: None)


def test_hdr_calibration_request_returns_hdr_calibration_response(dummy_improc_servicer):
    expected_g = [0.0, 1.0]
    dummy_improc_servicer.calibrate_hdr = lambda a, b: expected_g

    data = [PixelData() for _ in range(4)]
    request = HdrCalibrationRequest(LdrPixelData=data)
    response = dummy_improc_servicer.HdrCalibration(request, None)
    np.testing.assert_equal(response.g, expected_g)


def test_hdr_calibration_request_returns_hdr_calibration_response(dummy_improc_servicer):
    expected_g = [0.0, 1.0]
    dummy_improc_servicer.calibrate_hdr = lambda a, b: expected_g

    data = [PixelData() for _ in range(4)]
    request = HdrCalibrationRequest(LdrPixelData=data)
    response = dummy_improc_servicer.HdrCalibration(request, None)
    np.testing.assert_equal(response.g, expected_g)


def test_hdr_calibration_raises_value_error_when_number_of_images_is_less_than_2(dummy_improc_servicer):
    data = [PixelData()]
    request = HdrCalibrationRequest(LdrPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.HdrCalibration(request, None)


def test_hdr_calibration_sets_g_to_servicer(dummy_improc_servicer):
    g = [0.0, 1.0]
    dummy_improc_servicer.calibrate_hdr = lambda a, b: g

    data = [PixelData(), PixelData()]
    request = HdrCalibrationRequest(LdrPixelData=data)
    dummy_improc_servicer.HdrCalibration(request, None)

    assert g == dummy_improc_servicer.g


def test_hdr_calibration_raises_value_error_when_all_rows_not_same(dummy_improc_servicer):
    data = [PixelData(Rows=1), PixelData(Rows=2)]
    request = HdrRequest(LdrPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Hdr(request, None)


def test_hdr_calibration_raises_value_error_when_all_columns_not_same(dummy_improc_servicer):
    data = [PixelData(Columns=1), PixelData(Columns=2)]
    request = HdrRequest(LdrPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Hdr(request, None)


def test_hdr_calibration_raises_value_error_when_all_bits_allocated_not_same(dummy_improc_servicer):
    data = [PixelData(BitsAllocated=8), PixelData(BitsAllocated=16)]
    request = HdrRequest(LdrPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Hdr(request, None)


def test_hdr_calibration_raises_value_error_error_when_all_bits_stored_not_same(dummy_improc_servicer):
    data = [PixelData(BitsStored=8), PixelData(BitsStored=16)]
    request = HdrRequest(LdrPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Hdr(request, None)


def test_hdr_exists_in_servicer(dummy_improc_servicer):
    data = [PixelData(BitsAllocated=1), PixelData(BitsAllocated=1)]
    dummy_improc_servicer.g = [0, 0]
    request = HdrRequest(LdrPixelData=data)
    dummy_improc_servicer.Hdr(request, None)


def test_hdr_raises_value_error_when_number_of_images_is_less_than_2(dummy_improc_servicer):
    data = [PixelData()]
    request = HdrRequest(LdrPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Hdr(request, None)


def test_hdr_raises_runtime_error_when_g_not_defined(dummy_improc_servicer):
    data = [PixelData(BitsAllocated=8), PixelData(BitsAllocated=8)]
    request = HdrRequest(LdrPixelData=data)
    with pytest.raises(RuntimeError):
        dummy_improc_servicer.Hdr(request, None)
        

def test_hdr_raises_value_error_when_all_rows_not_same(dummy_improc_servicer):
    data = [PixelData(Rows=1), PixelData(Rows=2)]
    request = HdrRequest(LdrPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Hdr(request, None)
        

def test_hdr_raises_value_error_when_all_columns_not_same(dummy_improc_servicer):
    data = [PixelData(Columns=1), PixelData(Columns=2)]
    request = HdrRequest(LdrPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Hdr(request, None)


def test_hdr_raises_value_error_when_all_bits_allocated_not_same(dummy_improc_servicer):
    data = [PixelData(BitsAllocated=8), PixelData(BitsAllocated=16)]
    request = HdrRequest(LdrPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Hdr(request, None)
        

def test_hdr_raises_value_error_when_all_bits_stored_not_same(dummy_improc_servicer):
    data = [PixelData(BitsStored=8), PixelData(BitsStored=16)]
    request = HdrRequest(LdrPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Hdr(request, None)


def test_hdr_returns_hdr_result(dummy_improc_servicer):
    hdr_result = np.full((10, 10), 10, dtype=np.uint8)
    dummy_improc_servicer.apply_hdr = lambda a, b, c: hdr_result
    dummy_improc_servicer.g = [0]
    data = [PixelData(), PixelData()]

    request = HdrRequest(LdrPixelData=data)
    result = dummy_improc_servicer.Hdr(request, None)

    hdr_result.tobytes() == result.HdrPixelData.PixelData


def test_aar_exists_in_servicer(dummy_improc_servicer):
    data = [PixelData(), PixelData()]
    request = AarRequest(AarPixelData=data)
    dummy_improc_servicer.Aar(request, None)


def test_aar_raises_value_error_when_all_rows_not_same(dummy_improc_servicer):
    data = [PixelData(Rows=1), PixelData(Rows=2)]
    request = AarRequest(AarPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Aar(request, None)


def test_aar_raises_value_error_when_all_columns_not_same(dummy_improc_servicer):
    data = [PixelData(Columns=1), PixelData(Columns=2)]
    request = AarRequest(AarPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Aar(request, None)


def test_aar_raises_value_error_when_all_bits_allocated_not_same(dummy_improc_servicer):
    data = [PixelData(BitsAllocated=8), PixelData(BitsAllocated=16)]
    request = AarRequest(AarPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Aar(request, None)


def test_aar_raises_value_error_when_all_bits_stored_not_same(dummy_improc_servicer):
    data = [PixelData(BitsStored=8), PixelData(BitsStored=16)]
    request = AarRequest(AarPixelData=data)
    with pytest.raises(ValueError):
        dummy_improc_servicer.Aar(request, None)


def test_aar_returns_response_with_aar_result(dummy_improc_servicer):
    aar_result = np.full((10, 10), 10, dtype=np.uint8)
    dummy_improc_servicer.apply_aar = lambda a: aar_result
    data = [PixelData(), PixelData()]

    request = AarRequest(AarPixelData=data)
    result = dummy_improc_servicer.Aar(request, None)

    aar_result.tobytes() == result.AarPixelData.PixelData