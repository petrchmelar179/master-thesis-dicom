import time
import datetime
from service.improc_service import start_server
from config import service_port, service_ip


if __name__ == "__main__":
    print("Starting improc service server...")
    server = start_server(service_ip, service_port)
    print("Improc service server started.")

    try:
        while True:
            print(f"{datetime.datetime.now()} Periodical beat")
            time.sleep(5)
    except KeyboardInterrupt:
        server.stop(0)
