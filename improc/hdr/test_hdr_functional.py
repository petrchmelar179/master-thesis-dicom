import pytest
import numpy as np
import cv2
from matplotlib import pyplot as plt
from skimage.io import imsave

from glob import glob
from os.path import join
import re

from skimage.io import imread
from skimage.exposure import rescale_intensity, adjust_gamma
from skimage.transform import downscale_local_mean

from hdr.debevec_hdr import detector_response_solver, construct_radiance_map
from hdr.drago_tone_mapping import apply_drago_tone_mapping


@pytest.fixture()
def calibration_images(request):
    exposures = []
    images = []
    for path in glob(join('resources', 'calibration_images', '*')):
        image = imread(path)

        # resources data are taken with 14 bit ADC so it would be cool to rescale them
        rescaled_image = rescale_intensity(image, in_range=(0, 2**14 - 1), out_range=np.uint16)
        images.append(rescaled_image)

        # now parse ms exposures from path using regex expression
        match = re.search('\d+(?=ms)', path)
        exposures.append(int(match.group()) / 1000)
    return np.array(images), np.array(exposures)


@pytest.fixture()
def ldr_images(request):
    exposures = []
    images = []
    for path in glob(join('resources', 'ldr_images', '*')):
        image = imread(path)

        # resources data are taken with 14 bit ADC so it would be cool to rescale them
        rescaled_image = rescale_intensity(image, in_range=(0, 2**14 - 1), out_range=np.uint16)
        images.append(rescaled_image)

        # now parse ms exposures from path using regex expression
        match = re.search('\d+(?=ms)', path)
        exposures.append(int(match.group()) / 1000)
    return np.array(images), np.array(exposures)


def test_detector_response_solver_generate_response_data(calibration_images):
    images, exposures = calibration_images
    g, _ = detector_response_solver(images, exposures, 10)
    assert len(g) == 2**16


def test_construct_radiance_map_generate_radiance_map(calibration_images, ldr_images):
    calib_images, calib_exposures = calibration_images
    ldr_images, ldr_exposures = ldr_images

    g, _ = detector_response_solver(calib_images, calib_exposures, 1)

    downscaled_ldr_images = np.array([np.array(downscale_local_mean(image, (10, 10))) for image in ldr_images],
                                     dtype=np.uint16)
    construct_radiance_map(downscaled_ldr_images, ldr_exposures, g)


def test_tone_mapping_can_be_applied_on_hdr_radiance_map(calibration_images, ldr_images):
    calib_images, calib_exposures = calibration_images
    ldr_images, ldr_exposures = ldr_images
    g, _ = detector_response_solver(calib_images, calib_exposures, 1)

    downscaled_ldr_images = np.array([np.array(downscale_local_mean(image, (2, 2))) for image in ldr_images],
                                     dtype=np.uint16)
    radiance_map = construct_radiance_map(downscaled_ldr_images, ldr_exposures, g)
    tone_mapped_image = apply_drago_tone_mapping(radiance_map, 0.75)




