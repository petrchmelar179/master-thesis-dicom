import pytest
import numpy as np

from hdr.drago_tone_mapping import apply_drago_tone_mapping


def test_apply_drago_tone_mapping_exists():
    apply_drago_tone_mapping(np.full((10, 10), 1.0, dtype=np.float), 1)


def test_apply_drago_tone_mapping_raises_value_error_when_hdr_image_is_not_float():
    with pytest.raises(ValueError):
        apply_drago_tone_mapping(np.zeros(0, dtype=np.uint8), 1)


def test_apply_drago_tone_mapping_raises_value_error_when_hdr_image_has_wrong_dimensions():
    with pytest.raises(ValueError):
        apply_drago_tone_mapping(np.full((10, 10, 10), 1.0, dtype=np.float), 1)


def test_apply_drago_tone_mapping_returns_image_with_correct_dimensions():
    toned_image = apply_drago_tone_mapping(np.full((10, 10), 1.0, dtype=np.float), 1)
    np.testing.assert_equal(toned_image.shape, (10, 10))


def test_apply_drago_tone_mapping_returns_image_with_values_in_range_from_0_to_1():
    toned_image = apply_drago_tone_mapping(np.random.rand(100, 100) * 1000, 1)
    assert np.around(np.max(toned_image), 4) <= 1.0 and np.min(toned_image) >= 0

