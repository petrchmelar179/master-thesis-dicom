import pytest
import numpy as np
from hdr.debevec_hdr import detector_response_solver, sort_images_by_exposures_ascending, validate_images_exposures_pair, \
    w, construct_radiance_map


def test_response_solver_exists():
    detector_response_solver(np.zeros((2, 3, 3), dtype=np.uint8), np.arange(1, 3), 1)


def test_response_solver_number_of_exposures_not_equal_to_number_of_images_raises_value_error():
    with pytest.raises(ValueError):
        detector_response_solver(np.zeros((10, 20, 20)), np.arange(1, 10), 1)


def test_response_solver_raises_value_error_when_images_has_wrong_dimensions():
    with pytest.raises(ValueError):
        detector_response_solver(np.zeros((10, 20)), np.zeros((9, 1)), 1)


def test_response_solver_raises_value_error_when_not_uin8_or_uint16():
    with pytest.raises(ValueError):
        detector_response_solver(np.zeros((10, 20, 20), dtype=np.uint32), np.arange(1, 10), 1)


def test_response_solver_returns_exposure_with_correct_dimensions():
    g, _ = detector_response_solver(np.full((2, 20, 20), 5, dtype=np.uint8), np.arange(1, 3), 1)
    assert g.size == 2**8


def test_response_solver_returns_exposure_with_correct_dimensions():
    _, log_E = detector_response_solver(np.full((2, 20, 20), 5, dtype=np.uint8), np.arange(1, 3), 1)
    assert log_E.size == 200


def test_response_solver_returns_2_power_16_values_for_log_exposure_when_input_images_are_uint16():
    g, _ = detector_response_solver(np.full((2, 20, 20), 10000, dtype=np.uint16), np.arange(1, 3), 1)
    assert g.size == 2**16


def test_sort_files_by_exposure_exists():
    sort_images_by_exposures_ascending(np.zeros((1, 0, 0), np.uint8), np.ones(1))


def test_sort_images_by_exposures_ascending_number_of_exposures_not_equal_to_number_of_images_raises_value_error():
    with pytest.raises(ValueError):
        sort_images_by_exposures_ascending(np.zeros((10, 20, 20), dtype=np.uint8), np.zeros((9, 1)))


def test_sort_images_by_exposures_ascending_raises_value_error_when_images_has_wrong_dimensions():
    with pytest.raises(ValueError):
        sort_images_by_exposures_ascending(np.zeros((10, 20), dtype=np.uint8), np.zeros((9, 1)))


def test_sort_images_by_exposures_ascending_raises_value_error_when_not_uin8_or_uint16():
    with pytest.raises(ValueError):
        sort_images_by_exposures_ascending(np.zeros((10, 20), dtype=np.uint32), np.zeros((9, 1)))


def test_sort_images_by_exposures_ascending_returns_sorted_exposures():
    first_exposure = 10
    second_exposure = 2
    third_exposure = 5

    _, exposures = sort_images_by_exposures_ascending(
        np.array([np.zeros((2, 2)), np.zeros((2, 2)), np.zeros((2, 2))], dtype=np.uint8),
        np.array([first_exposure, second_exposure, third_exposure]))

    np.testing.assert_equal(exposures, np.array([second_exposure, third_exposure, first_exposure]))


def test_sort_images_by_exposures_ascending_returns_sorted_images():
    first_exposure = 10
    second_exposure = 2
    third_exposure = 5

    first_array = np.full((1, 1), 10)
    second_array = np.full((1, 1), 2)
    third_array = np.full((1, 1), 5)

    images, _ = sort_images_by_exposures_ascending(
        np.array([first_array, second_array, third_array], dtype=np.uint8),
        np.array([first_exposure, second_exposure, third_exposure]))

    np.testing.assert_equal(images, np.array([second_array, third_array, first_array], dtype=np.uint8))


def test_validate_images_exposures_pair_exists():
    validate_images_exposures_pair(np.zeros((1, 0, 0), dtype=np.uint8), np.zeros(1))


def test_validate_images_exposures_pair_number_of_exposures_not_equal_to_number_of_images_raises_value_error():
    with pytest.raises(ValueError):
        validate_images_exposures_pair(np.zeros((10, 20, 20), dtype=np.uint8), np.zeros((9, 1)))


def test_validate_images_exposures_pair_raises_value_error_when_images_has_wrong_dimensions():
    with pytest.raises(ValueError):
        validate_images_exposures_pair(np.zeros((10, 20), dtype=np.uint8), np.zeros((9, 1)))


def test_validate_images_exposures_pair_raises_value_error_when_image_has_float_pixels():
    with pytest.raises(ValueError):
        validate_images_exposures_pair(np.zeros((10, 20, 20), dtype=float), np.zeros((10, 1)))


def test_validate_images_exposures_pair_raises_value_error_when_not_uin8_or_uint16():
    with pytest.raises(ValueError):
        validate_images_exposures_pair(np.zeros((10, 20, 20), dtype=np.uint32), np.zeros((10, 1)))


def test_w_exists():
    w(0, 0, 0)


def test_construct_radiance_map_exists():
    construct_radiance_map(np.zeros((10, 20, 20), dtype=np.uint8), np.arange(1, 11), np.zeros(2**8))


def test_construct_radiance_map_number_of_exposures_not_equal_to_number_of_images_raises_value_error():
    with pytest.raises(ValueError):
        construct_radiance_map(np.zeros((10, 20, 20), dtype=np.uint8), np.zeros((9, 1)), np.zeros(2**8))


def test_construct_radiance_map_raises_value_error_when_images_has_wrong_dimensions():
    with pytest.raises(ValueError):
        construct_radiance_map(np.zeros((10, 20), dtype=np.uint8), np.arange(1, 11), np.zeros(2**8))


def test_construct_radiance_map_raises_value_error_when_g_does_not_have_correct_length():
    with pytest.raises(ValueError):
        construct_radiance_map(np.zeros((10, 20, 20), dtype=np.uint8), np.arange(1, 11), np.zeros(1))


def test_construct_radiance_map_returns_map_with_correct_shape():
    radiance_map = construct_radiance_map(np.zeros((10, 20, 20), dtype=np.uint8), np.arange(1, 11), np.zeros(2**8))
    np.testing.assert_equal(radiance_map.shape, (20, 20))

