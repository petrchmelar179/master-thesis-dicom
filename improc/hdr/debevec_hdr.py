import numpy as np
from skimage.exposure import rescale_intensity
from scipy.interpolate import UnivariateSpline
import matplotlib.pyplot as plt
import cv2


def detector_response_solver(images, exposures, smoothness_constant):
    validate_images_exposures_pair(images, exposures)
    number_of_images = images.shape[0]
    number_of_pixels = 200
    Z_max = np.iinfo(np.uint8).max
    Z_min = np.iinfo(np.uint8).min

    # convert images array to uint8 - it has to be done because memory limitations
    # for uint16 there could be 2**16 (Z_max_16) + number_of_pixels equations to solve
    rescaled_images = np.zeros_like(images, dtype=np.uint8)
    for idx, image in enumerate(images):
        rescaled_images[idx, ...] = rescale_intensity(image, in_range=image.dtype.name, out_range=np.uint8)

    # sort images and exposures, returned values are E for exposures and Z for images
    sorted_images, sorted_exposures = sort_images_by_exposures_ascending(rescaled_images, exposures)
    # calculate natural log of exposures (ln_E) and flatten images pixels so image will be 1-D vector (Z)
    ln_t = np.log(sorted_exposures)
    random_idxs = np.random.choice(sorted_images[0].size, number_of_pixels)
    Z = np.array([image.flatten()[random_idxs] for image in sorted_images])

    # allocate A matrix of b = Ax system where x = [g(Zij), ln(E)]
    # the x vector contains all possible g and lE values (for all pixels in all images)
    A = np.zeros((number_of_pixels * number_of_images + Z_max + 1, Z_max + 1 + number_of_pixels))

    # allocate b matrix of b = Ax which is represented by natural log of exposure constants multiplied by
    # weighted value
    # it has to have same number of rows as A and one column
    B = np.zeros((A.shape[0], 1))

    # iterate over pixel vectors in images set and setup g(z) and ln(E) multipliers for A and B matrix
    counter = 0
    for pixel_idx in range(0, number_of_pixels):
        for image_idx in range(0, number_of_images):
            pixel_value = Z[image_idx, pixel_idx]
            weighted_pixel_value = w(pixel_value, Z_max, Z_min)
            # setup g(z) multiplier
            A[counter, pixel_value] = weighted_pixel_value
            # setup ln(E) multiplier
            A[counter, Z_max + pixel_idx] = -weighted_pixel_value
            # setup log delta t multiplied by weighted pixel value
            B[counter, 0] = weighted_pixel_value * ln_t[image_idx]
            counter = counter + 1

    # fix curve by setting its middle to 0
    A[counter, int(Z_max / 2)] = 1
    counter = counter + 1

    # Add smoothness equation
    for pixel_value in range(Z_min + 1, Z_max):
        weighted_pixel_value = w(pixel_value, Z_max, Z_min)
        A[counter, pixel_value - 1] = smoothness_constant * weighted_pixel_value
        A[counter, pixel_value] = -2 * smoothness_constant * weighted_pixel_value
        A[counter, pixel_value + 1] = smoothness_constant * weighted_pixel_value
        counter = counter + 1

    # solve system and slice result vector to result
    x = np.linalg.lstsq(A, B, rcond=None)[0]
    g = x[:Z_max + 1]
    log_E = x[Z_max + 1:]
    
    # now we need to interpolate g values in case we converted passed images to uint8 bit depth
    if images.dtype != np.uint8:
        spline = UnivariateSpline(np.arange(Z_max + 1), g, k=3, s=0)
        g_interpolated = spline(np.linspace(0, Z_max, np.iinfo(images.dtype).max + 1))
        return g_interpolated, log_E
    else:
        return g, log_E


def construct_radiance_map(images, exposures, g):
    validate_images_exposures_pair(images, exposures)
    number_of_images = images.shape[0]
    number_of_pixels = images[0].size
    image_max = np.iinfo(images.dtype).max
    image_min = np.iinfo(images.dtype).min

    if np.iinfo(images.dtype).max + 1 != len(g):
        raise ValueError(f'g has wrong size. Images with {images.dtype} requires g with '
                         f'{np.iinfo(images.dtype).max + 1} elements. (current {len(g)})')

    # sort images and exposures, returned values are E for exposures and Z for images
    sorted_images, sorted_exposures = sort_images_by_exposures_ascending(images, exposures)

    # calculate natural log of exposures (ln_E) and flatten images pixels so image will be 1-D vector (Z)
    ln_t = np.log(sorted_exposures)
    Z = np.reshape(sorted_images, (number_of_images, number_of_pixels))
    ln_E = np.zeros(number_of_pixels)
    w_lookup = np.array([w(pixel_value, image_max, image_min) for pixel_value in range(image_max + 1)])

    for pixel_idx in range(number_of_pixels):
        pixel_values = Z[:, pixel_idx]

        g_Zi = g[pixel_values]
        w_Zi = w_lookup[pixel_values]

        numerator = np.dot(w_Zi, g_Zi - ln_t)
        denominator = np.sum(w_Zi)
        ln_E[pixel_idx] = numerator / denominator

    E = np.float_power(np.full_like(ln_E, np.e), ln_E)
    return np.reshape(E, images[0].shape)


def w(z, Z_max, Z_min):
    return z - Z_max if z <= 0.5 * (Z_min + Z_max) else Z_max - z


def sort_images_by_exposures_ascending(images, exposures):
    validate_images_exposures_pair(images, exposures)
    sorted_idx = exposures.argsort()
    sorted_exposures = exposures[sorted_idx]
    sorted_images = images[sorted_idx]
    return sorted_images, sorted_exposures


def validate_images_exposures_pair(images, exposures):
    if images.shape[0] != exposures.shape[0]:
        raise ValueError(
            f"number of images {images.shape[0]} must be same as a number of exposures {exposures.shape[0]}")

    if len(images.shape) != 3:
        raise ValueError(f"invalid images shape."
                         f" Required shape format is (image index, rows, columns) (current shape: {images.shape})")

    if images.dtype not in [np.uint16, np.uint8]:
        raise ValueError(f"invalid images pixel type. Only python or numpy unsigned integer is supported "
                         f"(current type: {images.dtype})")
