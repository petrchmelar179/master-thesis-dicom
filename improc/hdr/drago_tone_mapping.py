import numpy as np
import math


def apply_drago_tone_mapping(hdr_image, bias):
    if hdr_image.dtype not in [np.float, np.float64]:
        raise ValueError(f"invalid images pixel type. Only numpy float is supported "
                         f"(current type: {hdr_image.dtype})")

    if len(hdr_image.shape) != 2:
        raise ValueError(f"invalid hdr image shape."
                         f" Required shape format is (rows, columns) (current shape: {hdr_image.shape})")

    # L_dmax = 100 cd^m2 is common reference for CRT displays
    L_dmax = 100
    L_wmax = np.max(hdr_image)

    # compute constant multiplier
    multiplier = (L_dmax * 0.01) / math.log(L_wmax + 1, 10)

    # compute log(bias) / log(0.5)
    log_bias_log_0_5 = np.log(bias) / np.log(0.5)

    # flatten image and apply operator on it
    flatted_hdr_image = np.reshape(hdr_image, hdr_image.size)

    L_d = np.zeros_like(flatted_hdr_image)
    for L_w_idx in range(flatted_hdr_image.size):
        L_w = flatted_hdr_image[L_w_idx]

        L_d[L_w_idx] = multiplier * np.log(L_w + 1) / np.log(2 + np.power(L_w / L_wmax, log_bias_log_0_5) * 8)

    return L_d.reshape(hdr_image.shape)